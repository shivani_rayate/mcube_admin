//const ImdbData = require('../app/models/testimdb');

class ImdbCls {
    ImdbCls() { }


    save(params) {
        const ImdbDocument = new ImdbData({
            asset_id: params.assetid ? params.assetid : null,
            metadata: params.metadata ? params.metadata : null
        });

        console.log(`metadata SAVE BODY PARAMS :: ${JSON.stringify(params)}`);

        return new Promise((resolve, reject) => {
            ImdbDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        status: 200,
                        message: 'IMDB data saved',
                    };
                    resolve(response);
                }
            });
        });
    }


}


module.exports = {
    ImdbClass: ImdbCls,
};