/* eslint-disable no-param-reassign */
const moment = require('moment');
const _ = require('lodash');

const util = require('util');
const Video = require('../app/models/video');
const Contentid = require('../app/models/content_id');
const videoPartial = require('../app/utilities/video/index');

class VideoCls {

    VideoCls() { }
    save(params){

        return new Promise((resolve, reject) => {
            const videoDocument = new Video({
                content_id: params.content_id ? params.content_id : null,
                file_name: params.file_name ? params.file_name : null,
                video_id: params.fileName ? params.fileName : null,
                duration: params.duration ? params.duration : null,
                title: params.title ? params.title : null,
                summary_short: params.summary_short ? params.summary_short : null,
                status: params.status ? params.status : null,
                keywords: params.keywords ? params.keywords : null,
                language: params.language ? params.language : null,
                description: params.description ? params.description : null,
                cast: params.cast ? params.cast : null,
                is_featured: params.is_featured ? params.is_featured : null,
                is_popular: params.is_popular ? params.is_popular : null,

                thumbnail_pic: params.thumbnail_pic ? params.thumbnail_pic : null,

                created_by: 'Admin',
            });
            videoDocument.save((err, videoData) => {
                if (err) {
                    console.log(`err in saving video: ${err}`);

                    reject(err);
                } else {
                    console.log(`SAVED VIDEO DATA ${util.inspect(videoData)}`);
                    resolve(videoData);

                }
            });

        });

    }
 findall() {
        return new Promise((resolve, reject) =>{
            Video.find({})
                .sort({ created_at: -1 })
                .exec((err, videos) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all videos has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        
                        resolve(videos);
                    }
                });
        });
    }

    getContentId(series) {
        return new Promise((resolve, reject) => {
            Contentid.find({

            })
                .count()
                .exec((err, contentidCount) => {
                    if (err) {
                        reject(err);
                    } else {
                        const contentParams = {
                            count: contentidCount,
                        };
                        console.log(`contentParams>>>>> ${util.inspect(contentParams)}`);
                        videoPartial.getNextContentId(contentParams).then((content_id) => {
                            const contentId = series + content_id;
                            const contentid_document = new Contentid({
                                content_id: contentId,
                            });
                            contentid_document.save((err, _video) => {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve(contentId);
                                }
                            });
                        });
                    }
                });
        });
    }

  countTodaysVideo(){
        return new Promise((resolve, reject) => {

            let start = moment().startOf('day'); // set to 12:00 am today
            let end = moment().endOf('day'); // set to 23:59 pm today

            Video.find({ created_at: { $gte: start, $lt: end } })
                .countDocuments()
                .exec((err, Count) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(Count);
                    }
                });
        });
    }

    findStatusByid(params) {
        return new Promise((resolve, reject) => {
            Video.findOne({ content_id: params.content_id }).exec((err, data) => {
                if (err) {
                    console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    findDataByid(params){
        return new Promise((resolve, reject) => {
            Video.findOne({ content_id: params.content_id }).exec((err, data) => {
                if (err) {
                    console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }
    deletevideo(params) {

        return new Promise((resolve, reject) => {
            const obj = {};
            Video.deleteOne({ content_id: params.content_id })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE USER SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }

  findOneAndUpdate(params){
        return new Promise((resolve, reject) => {
            Video.findOne({
                content_id: params.content_id,
            }, (err, videoData) => {
                if (err) {
                    console.log(`err in findOne Video: ${util.inspect(err)}`);
                    reject(err);
                } else {

                    if (params.duration != null) {
                        videoData.duration = params.duration;
                    }
                    if (params.title != null) {
                        videoData.title = params.title;
                    }
                    if (params.summary_short != null) {
                        videoData.summary_short = params.summary_short;
                    }

                    if (params.media_job_status != null) {
                        videoData.media_job_status = params.media_job_status;
                    }

                    if (params.media_job_id != null) {
                        videoData.media_job_id = params.media_job_id;
                    }

                    if (params.preview_url != null) {
                        videoData.preview_url = params.preview_url;
                    }

                    if (params.video_path != null) {
                        videoData.video_path = params.video_path;
                    }

                    if (params.description != null) {
                        videoData.description = params.description;
                    }

                    if (params.keywords != null) {
                        videoData.keywords = params.keywords;
                    }

                    if (params.language != null) {
                        videoData.language = params.language;
                    }

                    if (params.cast != null) {
                        videoData.cast = params.cast;
                    }

                    if (params.is_featured != null) {
                        videoData.is_featured = params.is_featured;
                    }

                    if (params.is_popular != null) {
                        videoData.is_popular = params.is_popular;
                    }


                    if (params.dynamicInputGroupsIDs_arr) {
                        //console.log("array >>>", params.dynamicInputGroupsIDs_arr)
                        var _dynamicInputGroupsIDs_arr = params.dynamicInputGroupsIDs_arr;

                        for (var i = 0; i < _dynamicInputGroupsIDs_arr.length; i++) {

                            var newKey = _dynamicInputGroupsIDs_arr[i].id ? _dynamicInputGroupsIDs_arr[i].id : null

                            if (newKey != null) {
                                videoData[`${newKey}`] = params.newKey;
                            }
                        }
                    }

                    /////////////////////////////////////////////
                    if (params.basic_transcode_status != null) {
                        videoData.profiles.basic.status = params.basic_transcode_status;
                    }
                    if (params.basic_transcode_url != null) {
                        videoData.profiles.basic.url = params.basic_transcode_url;
                    }
                    if (params.standard_transcode_status != null) {
                        videoData.profiles.standard.status = params.standard_transcode_status;
                    }
                    if (params.standard_transcode_url != null) {
                        videoData.profiles.standard.url = params.standard_transcode_url;
                    }
                    if (params.premium_transcode_status != null) {
                        videoData.profiles.premium.status = params.premium_transcode_status;
                    }
                    if (params.premium_transcode_url != null) {
                        videoData.profiles.premium.url = params.premium_transcode_url;
                    }
                    if (params.custom_transcode_status != null) {
                        videoData.profiles.custom.status = params.custom_transcode_status;
                    }
                    if (params.custom_transcode_url != null) {
                        videoData.profiles.custom.url = params.custom_transcode_url;
                    }
                    /////////////////////////////////////////////

                    if (params.thumbnail_pic != null) {
                        videoData.thumbnail_pic = params.thumbnail_pic;
                    }
                    if (params.thumbnail != null) {
                        // eslint-disable-next-line valid-typeof
                        // eslint-disable-next-line no-unused-expressions
                        (typeof (params.thumbnail) === 'object' ? (videoData.thumbnail = params.thumbnail) : (videoData.thumbnail.push(params.thumbnail)));
                    }

                    videoData.save((errUpdate, videoResponseData) => {
                        if (errUpdate) {
                            console.log(`ERR IN UPDATE> ${JSON.stringify(errUpdate)}`);
                            reject(errUpdate);
                        } else {
                            resolve(videoResponseData);
                        }
                    });
                }



            });
        });
    }
}

module.exports = {
    VideoClass: VideoCls,
};

