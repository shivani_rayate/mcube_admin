const Profile = require('../app/models/profiles');


class ProfilesCls {
    ProfilesCls() { }

    save(params) {
        const profileDocument = new Profile({
            profile_name: params.profile_name ? params.profile_name : null,
            profiles: params.profiles ? params.profiles : null,
            mediaJSON: params.mediaJSON ? params.mediaJSON : null,
        });
        return new Promise((resolve, reject) => {
            profileDocument.save((err, data) => {
                if (err) {
                    console.error(`Error :: Mongo Profile Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    const response = {
                        code: 200,
                        message: data,
                    };
                    resolve(response);
                }
            });
        });
    }





    findall() {
        return new Promise((resolve, reject) => {
            Profile.find({})
                .sort({ _id: -1 })
                .exec((err, Profile) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all Profile has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        console.log(`Profile LENGTH> ${Profile.length}`);
                        resolve(Profile);
                    }
                });
        });
    }



    deleteProfile(params) {

        return new Promise((resolve, reject) => {
            const obj = {};
            Profile.deleteOne({ _id: params._id })
                .exec((err, data) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log(`DELETE Profile SUCCESS : ${JSON.stringify(data)}`);
                        obj.status = 200;
                        obj.data = data;
                        resolve(obj);
                    }
                });
        });
    }


    findDataByid(params) {
        return new Promise((resolve, reject) => {
            Profile.findOne({ profile_name: params.profile_name })
                .exec((err, data) => {
                    if (err) {
                        console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        resolve(data);
                    }
                });
        })
    }

    // find Media JSON
    findMediaJson(profile) {
        return new Promise((resolve, reject) => {
            Profile.find({ profile_name: profile })
                .exec((err, Profile) => {
                    if (err) {
                        console.error(`Error :: Mongo Find Profile has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        // console.log(`Profile > ${Profile}`);
                        resolve(Profile);
                    }
                });
        });
    }

}

module.exports = {
    ProfilesClass: ProfilesCls,
};