/* eslint-disable no-param-reassign */
const moment = require('moment');
const _ = require('lodash');
const util = require('util');
const dashboard = require('../app/models/video');
const Contentid = require('../app/models/content_id');
const Video = require('../app/models/video');

class DashboardCls {

    DashboardCls() { }

    save(params) {
        return new Promise((resolve, reject) => {
            const videoDocument = new Video({
                content_id: params.content_id ? params.content_id : null,
                file_name: params.file_name ? params.file_name : null,
                video_id: params.fileName ? params.fileName : null,
                duration: params.duration ? params.duration : null,
                title: params.title ? params.title : null,
                summary_short: params.summary_short ? params.summary_short : null,
                status: params.status ? params.status : null,
                keywords: params.keywords ? params.keywords : null,
                language: params.language ? params.language : null,
                description: params.description ? params.description : null,
                cast: params.cast ? params.cast : null,
                is_featured: params.is_featured ? params.is_featured : null,
                is_popular: params.is_popular ? params.is_popular : null,

                created_by: 'Admin',
            });
            console.log('SAVE');
            videoDocument.save((err, videoData) => {
                if (err) {
                    console.log(`err in saving video: ${err}`);
                    reject(err);
                } else {
                    console.log(`SAVED VIDEO DATA ${util.inspect(videoData)}`);
                    resolve(videoData);
                }
            });
        });
    }

    getContentId(series) {
        return new Promise((resolve, reject) => {
            Contentid.find({

            })
                .count()
                .exec((err, contentidCount) => {
                    if (err) {
                        reject(err);
                    } else {
                        const contentParams = {
                            count: contentidCount,
                        };
                        console.log(`contentParams>>>>> ${util.inspect(contentParams)}`);
                        videoPartial.getNextContentId(contentParams).then((content_id) => {
                            const contentId = series + content_id;
                            const contentid_document = new Contentid({
                                content_id: contentId,
                            });
                            contentid_document.save((err, _video) => {
                                if (err) {
                                    reject(err);
                                } else {
                                    resolve(contentId);
                                }
                            });
                        });
                    }
                });
        });
    }


    findall(){
        return new Promise((resolve, reject) => {
            Video.find({})
                .sort({ created_at: -1 })
                .exec((err, videos) => {
                    if (err) {
                        console.error(`Error :: Mongo Find all videos has error :: ${JSON.stringify(err)}`);
                        reject(err);
                    } else {
                        //console.log(`VIDEOS LENGTH> ${videos.length}`);
                        resolve(videos);
                    }
                });
        });
    }

    findStatusByid(params){
        return new Promise((resolve, reject) => {
         //  Video.find( { profiles:{basic:{status: "PROCESSING" }}} ).exec((err, data) => {
            Video.find( { $or: [ { "profiles.basic.status": "PROCESSING" } ,  { "profiles.standard.status": "PROCESSING" }, { "profiles.premium.status": "PROCESSING" }  ] }).exec((err, data) => {
            console.log(`success :: findStatusByid :: ${JSON.stringify(data)}`);
          
                if (err) {
                    console.error(`Error :: findStatusByid :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }


    findlistBystatus(){

        return new Promise((resolve, reject) => {
        

            Video.find({ $or: [ { "profiles.basic.status": "COMPLETE" } ,  { "profiles.standard.status": "COMPLETE" }, { "profiles.premium.status": "COMPLETE" }  ] }).exec((err, data) => {
                if (err) {
                    console.error(`Error :: findStatusByid :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    console.error("dashboard find list status >> >> " +JSON.stringify(data));
                    resolve(data);
                }
            });
        });
    }

    findDataByid(params){

        return new Promise((resolve, reject) => {
            Video.findOne({ content_id: params.content_id }).exec((err, data) => {
                if (err) {
                    console.error(`Error :: findDataByid :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

countTodaysVideo() {

        return new Promise((resolve, reject) => {

            let start = moment().startOf('day'); // set to 12:00 am today
            let end = moment().endOf('day'); // set to 23:59 pm today

            Video.find({ created_at: { $gte: start, $lt: end } })
            .exec((err, data) => {
                    if (err){
                      reject(err);
                    } else {
                       resolve(data);
                    }
                });
        });
    }

}

module.exports = {
    DashboardClass: DashboardCls,
};