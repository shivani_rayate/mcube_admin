const Config = require('../app/models/setting');

class SettingCls {
    SettingCls() { }


    save(params) {
        const configurationDocument = new Config({
            name: params.name ? params.name : null,
            config: params.config ? params.config : null,
        });
        return new Promise((resolve, reject) => {

            Config.findOne({
                name: params.name ? params.name : null,
            }, (err, ConfigData) => {

                console.log("ConfigData configuration >>> " + JSON.stringify(ConfigData))

                if (ConfigData != null) {
                    // UPDATE
                    if (params.name != null) {
                        ConfigData.name = params.name;
                    }
                    if (params.config != null) {
                        ConfigData.config = params.config;
                    }


                    ConfigData.save((errUpdate, ConfigResponseData) => {
                        if (errUpdate) {
                            console.log(`ERR IN UPDATE> ${JSON.stringify(errUpdate)}`);
                            reject(errUpdate);
                        } else {
                            console.log(`UPDATE NAME> ${JSON.stringify(ConfigResponseData)}`);
                            resolve(ConfigResponseData);
                        }
                    });


                } else {
                    // new insert
                    const configurationDocument = new Config({
                        name: params.name ? params.name : null,
                        config: params.config ? params.config : null,
                    });
                    configurationDocument.save((err, Data) => {
                        if (err) {
                            console.log(`err in saving config: ${err}`);
                            reject(err);
                        } else {
                            resolve(Data);
                        }
                    });


                }
            })
        });
    }

    findOneAndUpdate(params) {
        return new Promise((resolve, reject) => {
            Setting.findOne({
                name: params.name,
            }, (err, SettingData) => {
                if (err) {
                    console.error(`Error :: Mongo Save Error :: ${JSON.stringify(err)}`);
                    reject(err);
                } else {

                    SettingData.save((errUpdate, SettingResponseData) => {
                        if (errUpdate) {
                            console.log(`ERR IN UPDATE> ${JSON.stringify(errUpdate)}`);
                            reject(errUpdate);
                        } else {
                            console.log(`UPDATE Name> ${JSON.stringify(SettingResponseData)}`);
                            resolve(SettingResponseData);
                        }
                    });
                }
            });

        });
    }
}


module.exports = {
    SettingClass: SettingCls,
};