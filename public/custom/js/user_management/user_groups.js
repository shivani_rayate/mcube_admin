
var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};

var group_name = query('group-name') ? query('group-name') : null;


$(function () {
    $.get(`/v1/users/listUsersInGroup/${group_name}`,
        function (data, status) {
            if (data.status === 200) {

                console.log(JSON.stringify(data))
                destroyRowsofUsersInGroupTable()
                appendUsersInGroupsDatatable(data)
                $('#group_name').html(group_name);

            } else {
                toastr.error('Users Not Available');
            }
        });
})





function destroyRowsofUsersInGroupTable() {
    $('#list_usersInGroup_tbody').empty()
    $('#list_usersInGroup_table').DataTable().rows().remove();
    $("#list_usersInGroup_table").DataTable().destroy()
}




function appendUsersInGroupsDatatable(data) {

    var array = data.data.Users;
    console.log(JSON.stringify(array))
    if (array.length) {
        var options_table = "";
        array.forEach(function (element, i) {

            var Username = element.Username ? element.Username : "";
            var Attributes = element.Attributes ? element.Attributes : "";
            var UserCreateDate = element.UserCreateDate ? moment(element.UserCreateDate).format('lll') : "";
            var UserLastModifiedDate = element.UserLastModifiedDate ? moment(element.UserLastModifiedDate).format('lll') : "";
            var Enabled = element.Enabled ? element.Enabled : false;
            var UserStatus = element.UserStatus ? element.UserStatus : "UNCONFIRMED";
            var name = '';

            if (UserStatus === 'CONFIRMED') {
                UserStatus = '<span class="badge badge-success">Active</span>'
            } else {
                UserStatus = '<span class="badge badge-purple">Pending Activation</span>'
            }

            if (Attributes.length) {
                for (var j = 0; j < Attributes.length; j++) {
                    if (Attributes[j].Name === 'name') {
                        name = Attributes[j].Value;
                    }
                }
            }

            options_table += `<tr class="usersInGroup-tbl-row asset-row" id="${Username}">
               <td class="username">${Username}</td>
               <td class="name">${name}</td>
               <td class="state">${UserStatus}</td>
               <td class="action-td" id=${Username}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="dripicons-trash remove-user" ></i> </a>   </div></td>`;

            if (i == array.length - 1) {

                $('#list_usersInGroup_tbody').append(options_table)
               
                reInitializeUsergrouptable()
            }
        })
    } else {
        $('#list_usersInGroup_tbody').append(`<div><h6 class="text-primary">No users found.</h6></div>`);

    }

}


function reInitializeUsergrouptable() {
    $("#list_usersInGroup_table").DataTable().destroy()
    list_userno_group_table = $('#list_usersInGroup_table').DataTable({
        "order": [[1, "desc"]], // for descending order
        "columnDefs": [
            { "width": "35%", "targets": 0 },
            { "width": "30%", "targets": 1 }
        ]
    })

    $("#list_usersInGroup_tbody tr:first").addClass("active");

   
}



// Remove User From Group
$(document).on("click", ".remove-user", function () {

    var params = {
        username: $(this).closest(".action-td").attr('id'),
        groupname : $('#group_name').text(),
    }

    console.log(JSON.stringify(params))

    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, remove it!",
        cancelButtonText: "No, cancel!",
        confirmButtonClass: "btn btn-success mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if (t.value) {
            $.post('/v1/users/adminRemoveUserFromGroup',
                params,
                function (data, status) {
                    if (data.status === 200) {

                        toastr.success('User Removed From Group');

                        setTimeout(function () {
                            window.location.href = '/v1/users';
                        }, 3000)

                    } else if (data.status === 401) {
                        toastr.error('Operation Failed');
                    }
                });
        }

    })

})