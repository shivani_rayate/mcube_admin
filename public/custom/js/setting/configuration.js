$(document).ready(function () {

    $('#configuration').submit(function (event) {

        var ftp_config = {
            remoteServer: $("#remoteServer").val(),
            username: $("#userName2").val(),
            password: $("#password2").val(),
        }
        var s3_config = {
            firstname: $("#name2").val(),
            lastname: $("#surname2").val(),
            email: $("#email2").val(),
        }

        saveConfiguration(ftp_config, s3_config)
        event.preventDefault();

    })


    function saveConfiguration(ftp_config, s3_config) {

        var params = {
            name: $("#name").val(),
            config: {
                ftp: ftp_config,
                s3: s3_config
            },
        }

        $.ajax({
            url: '/v1/setting',
            type: 'POST',
            data: JSON.stringify(params),
            headers: {
                'Content-Type': "application/json",
            },
            'success': function (data, textStatus, request) {

                if (data.status == 200) {
                    showNotification('success', data.message)

                    setTimeout(function () {
                        window.location.href = '/v1/setting';
                    }, 2000)

                } else if (data.status == 401) {
                    showNotification('danger', data.message)
                }
            },
            'error': function (request, textStatus, errorThrown) {
                console.log("ERR IN SAVE: " + errorThrown)
            }
        })

    }

})