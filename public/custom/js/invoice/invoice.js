

function destroyRows(dataTblParams) {
    $(`#${dataTblParams.tblbody}`).empty()
    $(`#${dataTblParams.tblId}`).DataTable().rows().remove();
    $(`#${dataTblParams.tblId}`).DataTable().destroy()
}


// for formatting bytes to readable file size
function formatBytes(x) {

    const units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    let l = 0, n = parseInt(x, 10) || 0;

    while (n >= 1024 && ++l) {
        n = n / 1024;
    }
    //include a decimal point and a tenths-place digit if presenting 
    //less than ten of KB or greater units
    return (n.toFixed(n < 10 && l > 0 ? 1 : 0) + ' ' + units[l]);
}


// get all Tenant of tenant listing page
function tenantListing() {

    var maximumKey = 12;
    $.get(`/v1/admin/listUserPools?maxResults=${maximumKey}`,
        function (data, status) {
            // console.log(JSON.stringify(data))
            if (data.status == 200) {
                let tenant_data = data.data.UserPools;
                var tenantOptions = '';
                tenant_data.forEach(function (element, i) {
                    tenantOptions += `<option data-tenantName="${element.Name}" value="${element.Id}">${element.Name}</option>`
                })
                $('#tenant').append(tenantOptions);
            }
        })
}


// dynamic currency of USD ,INR call in costCalculation function
function currency() {
    return new Promise((resolve, reject) => {
        $.get('/v1/invoice/exchangeratesapi',

            function (data, status) {
                if (data) {

                    var currencyRates = {
                        _USD: data.rates.USD,
                        _INR: data.rates.INR
                    }
                    resolve(currencyRates)

                } else {
                    reject()
                }
            })
    })
}


// Billing  pdf send email
$(document).on("click", ".btn-send", function (e) {

    var doc = new jsPDF();
    var fileName = $("#select2-tenant-container").text()
    // var elementHTML = $('#invoice-div').html()
    // var elementHandler = {
    //     '#saveFile': function (element, renderer) {
    //         return true;
    //     }
    // };
    // doc.fromHTML(
    //     elementHTML, 15, 15,
    //     {
    //         'width': 300, 'elementHandlers': elementHandler
    //     });
    // // doc.save(`public/billingReport/${fileName}`);
    //  doc.save(fileName);

    doc.addHTML($('#invoice-div')[0], 15, 15, {
        'background': '#fff',
    }, function () {
        doc.save(fileName);
    });

    //  var path =`C:/Users/DELL/Downloads/${fileName}`

    // var params = {
    //                tenantName: $("#select2-tenant-container").text(),
    //                //pdf: path
    //             }
    // console.log(JSON.stringify(params))
    // $.post('/v1/invoice/emailSend',
    //     params,
    //     function (data, status) {
    //         if (data.status === 200) {
    //             toastr.success('Email Send successfully.');

    //             // setTimeout(function () {
    //             //    window.location.reload();
    //             // }, 3000)

    //         } else {
    //             toastr.danger('Email failed.')
    //         }

    //     });

})

// Tenant Invoice 
$(document).on("click", ".tenantInvoice", function () {

    var params = {
        tenantId: $("#tenant").val(),
        startDate: $('#reportrange').data('daterangepicker').startDate.format('YYYY-MM-DD'),
        endDate: $('#reportrange').data('daterangepicker').endDate.format('YYYY-MM-DD'),
    }

    var previewTotalCost, basicTotalCost, standardTotalCost, premiumTotalCost, totalStorageCost

    // billing data transfer

    $.post('/v1/invoice/dataTransferBilling',
        function (data, status) {
            if (data.status === 200) {
                console.log(JSON.stringify(data))
                console.log('data trasnfer success');
            } else {
                console.log('data transfer failed')
            }
        });

    // get all Assets from s3 bucket
    function getStorageCost(params) {

        return new Promise(async (resolve, reject) => {
            var currencyRates = await currency()
            $.get(`/v1/video/listS3_Assets/${params.tenantId}`,
                function (data, status) {
                    if (data.status == 200) {
                        
                        console.log('get storage cost >> >>' +JSON.stringify(data))

                        var dataTblParams = {

                            tblId: 'list_table', // data table ID
                            tblbody: 'list_tbody' // data table body ID
                        }
                        destroyRows(dataTblParams);
                        var options_table = "";
                        var bucketSize = 0;
                        var _data = data.data.Contents;
                        var rs_perGBperMonth = 1.65288;  // Rs. per GB per month
                        var rs_perGBperDay = rs_perGBperMonth / 30; // Rs. per GB per day
                        var index = 1;
                        for (var i = 0; i < _data.length; i++) {

                            fileSize = _data[i].Size;
                            LastModified = _data[i].LastModified;
                            bucketSize = fileSize + bucketSize;
                            totalBucketSize = formatBytes(bucketSize);
                            LastModifiedDate = new Date(LastModified)   //LastModified
                            var date = new Date()// date
                            var Difference_In_Time = date - LastModifiedDate;
                            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
                           
                            formatedFileSize = formatBytes(fileSize);
                            fileSizeUnit = formatedFileSize.slice(-2);
                            fileSizeUnitValue = formatedFileSize.slice(0, -2);
                            switch (fileSizeUnit) {

                                case `GB`:
                                    var GB = rs_perGBperDay;
                                    totalStorageCost = Difference_In_Days * fileSizeUnitValue * GB;
                                    console.log('GB >> >>' +totalStorageCost)
                                    break;
                                case `MB`:
                                    var MB = 0.00005380;
                                    totalStorageCost = Difference_In_Days * fileSizeUnitValue * MB;
                                    console.log('MB >> >>' +totalStorageCost)

                                    break;
                                case `KB`:
                                    var KB = 0.000000055096;
                                    totalStorageCost = Difference_In_Days * fileSizeUnitValue * KB;
                                    console.log('KB >> >>' +totalStorageCost)
                                    break;
                                default:
                            }
                       

                            if (i === _data.length - 1) {
                                options_table = '<tr class="invoice-tbl-row invoice-row invoice-table">' +
                                    '<td width="5%">' + index + '</td>' +
                                    '<td class="asset-name">s3 Storage</td>' +
                                    '<td width="20%">' + totalBucketSize + '</td>' +
                                    '<td width="20%" span class="WebRupee">Rs.</span>'+(totalStorageCost.toFixed(2))+ '</td>'
                                index++
                                $('#list_tbody').append(options_table)

                                resolve()
                            }
                        }
                    }
                });
        })

    }

    //get transcode cost
    function getTranscodeCost(params) {

        return new Promise(async (resolve, reject) => {

            var currencyRates = await currency()

            $.post(`/v1/invoice/getProfiles`,
                params,
                function (data, status) {
                    
                    console.log('get Transcode cost >> >>' +JSON.stringify(data))


                    if (data.status === 200) {

                        var dataTblParams = {
                            tblId: 'list_transcode_table', // data table ID
                            tblbody: 'list_transcode_tbody' // data table body ID
                        }
                        destroyRows(dataTblParams);

                        _data = data.data
                        var previewAssets = _data[0].Items
                        var basicProfileAssets = _data[1].Items
                        var standardProfileAssets = _data[2].Items
                        var premiumProfileAssets = _data[3].Items
                        var totalvideolengthminutes = 0;
                        var resolution = 0.0075 / currencyRates._USD;
                        var ResolutionInRs = resolution * currencyRates._INR;
                        var framerate = 25;
                        var index = 1;

                        for (var i = 0; i < _data[0].Count; i++) {

                            previewAssetsData = previewAssets[i].videolengthminutes ? previewAssets[i].videolengthminutes : 0
                            totalvideolengthminutes += parseInt(previewAssetsData)

                        }
                        previewTotalCost = ResolutionInRs * framerate * totalvideolengthminutes;
                        options_table = '<tr>' +
                            '<td width="5%">' + index + '</td>' +
                            '<td>preview</td>' +
                            '<td width="20%">' + totalvideolengthminutes + '<span> Mins</span></td>' +
                            '<td width="20%" span class="WebRupee">Rs.</span>' + (previewTotalCost.toFixed(2)) + '</td>'
                        index++
                        $('#list_transcode_tbody').append(options_table)

                        for (var j = 0; j < _data[1].Count; j++) {

                            basicAssetsData = basicProfileAssets[j].videolengthminutes ? basicProfileAssets[j].videolengthminutes : 0
                            totalvideolengthminutes += parseInt(basicAssetsData)

                        }

                        basicTotalCost = ResolutionInRs * framerate * totalvideolengthminutes;
                        options_table = '<tr>' +
                            '<td width="5%">' + index + '</td>' +
                            '<td> Basic </td>' +
                            '<td width="20%">' + totalvideolengthminutes + '<span> Mins</span></td>' +
                            '<td width="20%" span class="WebRupee">Rs.</span>' + (basicTotalCost.toFixed(2)) + '</td>'
                        index++
                        $('#list_transcode_tbody').append(options_table)

                        for (var k = 0; k < _data[2].Count; k++) {

                            standardAssetsData = standardProfileAssets[k].videolengthminutes ? standardProfileAssets[k].videolengthminutes : 0
                            totalvideolengthminutes += parseInt(standardAssetsData)
                        }
                        standardTotalCost = ResolutionInRs * framerate * totalvideolengthminutes;
                        options_table = '<tr>' +
                            '<td width="5%">' + index + '</td>' +
                            '<td> Standard </td>' +
                            '<td>' + totalvideolengthminutes + '<span> Mins</span></td>' +
                            '<td width="20%" span class="WebRupee">Rs.</span>' + (standardTotalCost.toFixed(2)) + '</td>'
                        index++
                        $('#list_transcode_tbody').append(options_table)

                        for (var l = 0; l < _data[3].Count; l++) {

                            premiumAssetsData = premiumProfileAssets[l].videolengthminutes ? premiumProfileAssets[l].videolengthminutes : 0
                            videoLength = parseInt(premiumAssetsData)
                            totalvideolengthminutes += videoLength
                        }
                        premiumTotalCost = ResolutionInRs * framerate * totalvideolengthminutes;
                        options_table = '<tr>' +
                            '<td width="5%">' + index + '</td>' +
                            '<td> premium </td>' +
                            '<td>' + totalvideolengthminutes + '<span> Mins</span></td>' +
                            '<td width="20%" span class="WebRupee">Rs.</span>' + (premiumTotalCost.toFixed(2)) + '</td>'
                        index++
                        $('#list_transcode_tbody').append(options_table)

                        setTimeout(function () {
                            resolve()
                        }, 500);

                    }
                    else {
                        toastr.error(data.message);
                    }
                });
        })
    }

    const promise1 = getStorageCost(params);
    const promise2 = getTranscodeCost(params);

    Promise.all([promise1, promise2]).then(function (response) {

        var totalTranscodprofiles = previewTotalCost + basicTotalCost + standardTotalCost + premiumTotalCost;
        var total = totalTranscodprofiles + totalStorageCost
        $("#total").html(`INR ` + (total.toFixed(2)))

        $('#invoice-div').show();
        $('.tenant-name').html(`<strong>Name: </strong>` + $("#tenant option:selected").text())
    })
})

// get of Tenant listing call 
$(function () {

    tenantListing();
    $('#invoice-div').hide()
})
