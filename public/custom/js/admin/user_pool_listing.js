
var maximumKey = 12;

$.get(`/v1/admin/listUserPools?maxResults=${maximumKey}`,

    function (data, status) {
        if (data.status === 200) {

            let _data = data.data;
            var option = '';
            for (var i = 0; i < _data.UserPools.length; i++) {
                name = _data.UserPools[i].Name;
                Id = _data.UserPools[i].Id;
                option += `<div class="col-md-3"><div id='${Id}' class="card border border-primary tenant-div text-center text-primary"><div class="p-1 tenant-name">${name}</div></div></div>`
            }
            $('#listUser').append(option)

        } else {
            console.log("failed")
            $('#listUser').append('<h2>No user pool found</h2>')

        }
    })



$(document).on("click", ".tenant-div", function () {
    userPoolId = $(this).attr('id');
    window.location.href = `/v1/admin/tenantInfo?userPoolId=${userPoolId}`;
})