

function openCreateUserPool(evt, Name) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(Name).style.display = "block";
    evt.currentTarget.className += "active";
}

document.getElementById("defaultOpen").click();


// add tag
$(document).on("click", "#addTag", function (e) {

    $('#add-tag-form-div').append(`<div class="form-group row"> 
    <div class="col-md-5 offset-sm-2 "> 
    <div class="form-group">
    <label class="col-form-label" for="tagKey">Tag Key</label>
    <input id="tagKey" name="tagKey" placeholder="Required"
     class="form-control" type="text" required></div></div>
     <div class="col-md-3"> 
    <div class="form-group">
    <label class="col-form-label" for="tagValue">Tag Value</label>
    <input id="tagValue" name="tagValue" placeholder="Required"
     class="form-control" type="text" required></div></div></div>`)
    e.preventDefault()

})


$(document).on("click", ".create-user-pool", function (e) {

    var params = {
        poolName: $("#pool_name").val(),
        modules: {
            mcube_adv_transcode: $('#mcube_adv_transcode').is(':checked'),
            mcube_data_science: $('#mcube_data_science').is(':checked'),
        }
     }

    $.post(`/v1/admin/createUserPoolApp`,
        params,
        function (data, status) {

            if (data.status === 200) {

                toastr.success('Tenant created successfully');
                setTimeout(function () {
                    window.location.href = '/v1/admin/tenants';
                }, 3000)

                // config update call will be here
                let params = {
                    config: data.config
                }
                $.post(`/config/addTenantConfig`,
                    params,
                    function (data, status) {
                        if (data.status === 200) {
                            console.log('Tenant config updated');
                        } else {
                            console.log('Tenant config update failed');
                        }
                    });

            } else {
                toastr.error(data.message);
            }
        });
})





// ###########################################################################

// $(document).on("click", ".next-step", function () {
//     var params = [];
//     params.push({
//         poolName: $("#pool_name").val()

//     })
//     var signIn = [{
//         username: $("#checkbox1").is(':checked'),
//         email: $("#checkbox2").is(':checked'),
//     }]
//     params.push(signIn)

//     var requiredAttribute = [{

//         address: $("#checkbox3").is(':checked'),
//         birthdate: $("#checkbox4").is(':checked'),
//         email: $("#checkbox5").is(':checked'),
//         familyName: $("#checkbox6").is(':checked'),
//         gender: $("#checkbox7").is(':checked'),
//         givenName: $("#checkbox8").is(':checked'),
//         nickName: $("#checkbox9").is(':checked'),
//         phoneNumber: $("#checkbox10").is(':checked'),
//         picture: $("#checkbox11").is(':checked'),
//         profile: $("#checkbox12").is(':checked'),
//         website: $("#checkbox13").is(':checked'),
//         updatedAt: $("#checkbox14").is(':checked'),
//     }]
//     params.push(requiredAttribute)
//     params.push({
//         minimumStrength: $("#length").val()

//     })
//     var passwordPolicy = [{
//         requiredNumbers: $("#checkbox15").is(':checked'),
//         requiredSpecialCharacters: $("#checkbox16").is(':checked'),
//         requiredUppecase: $("#checkbox17").is(':checked'),
//         requiredLowercase: $("#checkbox18").is(':checked'),

//     }]
//     params.push(passwordPolicy)
//     params.push({
//         daysToExpire: $("#expire").val()

//     })

//     var multifactorAuthentication = [{
//         off: $("#radio1").is(':checked'),
//         optional: $("#radio2").is(':checked'),
//         required: $("#radio3").is(':checked'),

//     }]
//     params.push(multifactorAuthentication)

//     var verification = [{
//         email: $("#radio4").is(':checked'),
//         phoneNumber: $("#radio5").is(':checked'),
//         emailOrPhone: $("#radio6").is(':checked'),
//         noVerification: $("#radio7").is(':checked'),

//     }]
//     params.push(verification)
//     params.push({
//         newRoleName: $("#roleName").val()

//     })

//     var amazonSesConfiguration = [{

//         email: $("#radio8").is(':checked'),
//         phoneNumber: $("#radio9").is(':checked'),

//     }]
//     params.push(amazonSesConfiguration)

//     var emailVerification = [{

//         code: $("#radio10").is(':checked'),
//         link: $("#radio11").is(':checked'),
//         emailSubject: $("#emailSubject").val(),
//         emailMessage: $("#emailMessage").val()


//     }]
//     params.push(emailVerification)
//     // var tag = [{
//     //     tagKey: $("#tagKey").val(),
//     //     tagValue: $("#tagValue").val()

//     // }]
//    params.push({

//         appClientName: $("#appClientName").val(),
//         refreshTokenExpiration: $("#refreshTokenExpiration").val()
//       })


//     console.log(JSON.stringify(params))
//       //alert(JSON.stringify(params))
//   })