var query = function (field, url) {
    var href = url ? url : window.location.href;
    var reg = new RegExp('[?&]' + field + '=([^&#]*)', 'i');
    var string = reg.exec(href);
    return string ? string[1] : null;
};
var userPoolId = query('userPoolId') ? query('userPoolId') : null;



$.get(`/v1/admin/describeUserPool?userPoolId=${userPoolId}`,
    function (data, status) {
        if (data.status === 200) {
            formatTenantInfo(data.data)
        } else {
            console.log("get describeUserPool Failed")
        }
    })


$.get(`/v1/admin/listAdminInTenant?userPoolId=${userPoolId}`,
    function (data, status) {
        if (data.status === 200) {
            destroyRowsTenantAdminTable();
            appendTenantAdminDatatable(data.data)
        } else {
            console.log("listAdminInTenant Failed!")
        }
    })


function destroyRowsTenantAdminTable() {
    $('#list_all_admin_tbody').empty()
    $('#list_all_admin_table').DataTable().rows().remove();
    $("#list_all_admin_table").DataTable().destroy()
}

var createAdminUserFlag;

function appendTenantAdminDatatable(data) {
    var array = data.Users;
    if (array.length) {

        // Admin is present >> to disable create admin button
        createAdminUserFlag = true;

        var options_table = "";
        array.forEach(function (element, i) {
            var Username = element.Username ? element.Username : "";
            var Attributes = element.Attributes ? element.Attributes : "";
            var UserStatus = element.UserStatus ? element.UserStatus : "UNCONFIRMED";
            var name = '';

            if (UserStatus === 'CONFIRMED') {
                UserStatus = '<span class="badge badge-success">Active</span>'
            } else {
                UserStatus = '<span class="badge badge-purple">Pending Activation</span>'
            }

            if (Attributes.length) {
                for (var j = 0; j < Attributes.length; j++) {
                    if (Attributes[j].Name === 'name') {
                        name = Attributes[j].Value;
                    }
                }
            }

            options_table += `<tr class="users-tbl-row asset-row" id="${Username}">
            <td class="username">${Username}</td>
            <td class="name">${name}</td>
            <td class="state">${UserStatus}</td>
            <td class="action-td" id=${Username}><div class="dropdown"> <a href="#" class="dropdown-toggle arrow-none card-drop" data-toggle="dropdown" aria-expanded="false"> <i class="fe-settings noti-icon"></i> </a> <div class="dropdown-menu dropdown-menu-right"><a href="#" class="dropdown-item delete-user">Delete</a> </div> </div></td>`;


            if (i == array.length - 1) {
                $('#list_all_admin_tbody').append(options_table)
                reInitializeDataTable()
            }
        })
    } else {
        $('#list_all_admin_tbody').append(`<div><h6 class="text-primary">No users found.</h6></div>`);
    }
}


function reInitializeDataTable() {
    $("#list_all_admin_table").DataTable().destroy()
    list_all_user_table = $('#list_all_admin_table').DataTable({
        //"order": [[1, "desc"]], // for descending order
        "columnDefs": [
            { "width": "35%", "targets": 0 },
            { "width": "30%", "targets": 1 }
        ]
    })

    $("#list_all_admin_table tbody tr:first").addClass("active");
}


function formatTenantInfo(tenantInfo) {

    //console.log("\n \n > "+JSON.stringify(tenantInfo))

    $('#tenantInfo').html(`
    <dt class='col-md-3 text-right text-primary'>Pool Id</dt><dd id="poolId" class='col-md-9' data-poolId=${tenantInfo.UserPool.Id}>${tenantInfo.UserPool.Id}</dd>
    <dt class='col-md-3 text-right text-primary'>Pool Name</dt><dd id="poolName" class='col-md-9 poolName' data-poolName=${tenantInfo.UserPool.Name}>${tenantInfo.UserPool.Name}</dd>
    <dt class='col-md-3 text-right text-primary'>Pool ARN</dt><dd class='col-md-9'>${tenantInfo.UserPool.Arn}</dd>
    <dt class='col-md-3 text-right text-primary'>Estimated number of users</dt><dd class='col-md-9'>${tenantInfo.UserPool.EstimatedNumberOfUsers}</dd>
    <dt class='col-md-3 text-right text-primary'>Required attributes</dt><dd class='col-md-9'></dd>
    `)

}



// Register
$("#register-form").submit(function (event) {

    $.post('/v1/admin/register',
        {
            userPoolId: userPoolId,
            name: $('#fullName').val().trim(),
            email: $('#email').val().trim().toLowerCase(),
            password: $("#password").val().trim()
        },
        function (data, status) {

            if (data.status === 200) {
                user = data.data.user;
                toastr.success('User registered successfully.');

                setTimeout(function () {
                    window.location.reload();
                }, 3000)

            } else {
                toastr.danger('User registration failed.')
            }

        });
    event.preventDefault()
})



// code for show hide button of userCreate & groupCreate 
$(document).ready(function () {

    $('#createbtn').hide(0);  //or do it through css
    $('#usertab').click(function () {
        $('#deletebtn').hide(0);
        if (!createAdminUserFlag) {
            $('#createbtn').show(0);
        }
    });

    //otherTab is the class for the tabs other than tab1
    $('#infotab').click(function () {
        $('#createbtn').hide(0);
        $('#deletebtn').show(0);

    });

});


// Delete tenant 
$(document).on("click", ".delete-tenant-btn", function (e) {

    var params = {
        userPoolId: userPoolId,
        poolName: $('#poolName').attr("data-poolName")
    }


    Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel!",
        confirmButtonClass: "btn btn-success mt-2",
        cancelButtonClass: "btn btn-danger ml-2 mt-2",
        buttonsStyling: !1
    }).then(function (t) {
        if (t.value) {

            $.post(`/v1/admin/deleteTenant`,
                params,
                function (data, status) {
                   
                    console.log('Tenant deleted >> >> ' + JSON.stringify(data))
                    if (data.status === 200) {
                        toastr.success('Tenant deleted successfully');
                        setTimeout(function () {
                            window.location.href = '/v1/admin/tenants';
                        }, 3000)

                        $.post(`/config/deleteTenantConfig`,
                            params,
                            function (data, status) {
                                console.log('Tenant config deleted >> >> ' + JSON.stringify(data))
                                if (data.status === 200) {
                                    console.log('Tenant config deleted');
                                } else {
                                    console.log('Tenant config deleted failed');
                                }
                            });

                    } else {
                        toastr.success('Tenant deleted failed');
                    }
                });
        }
    })
    e.preventDefault()

})
