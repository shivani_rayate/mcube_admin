$(function () {
    // get all assets
    $.get('/v1/video/fetch_all_videos',
        function (data, status) {
            if (data.status == 200) {

                let _data = data.data;

                var basicProfilesAssets = 0;
                var standardProfilesAssets = 0;
                var premiumProfilesAssets = 0;
                var customProfilesAssets = 0;
                var inProgressAssets = 0;
                for (i = 0; i < _data.length; i++) {

                    if (_data[i].profiles.basic.status === 'COMPLETE') {
                        basicProfilesAssets++;
                    }

                    if (_data[i].profiles.standard.status === 'COMPLETE') {
                        standardProfilesAssets++;
                    }

                    if (_data[i].profiles.premium.status === 'COMPLETE') {
                        premiumProfilesAssets++;
                    }

                    if (_data[i].profiles.custom.status === 'COMPLETE') {
                        customProfilesAssets++;
                    }

                    if (_data[i].profiles.basic.status === 'PROCESSING' || _data[i].profiles.premium.status === 'PROCESSING' || _data[i].profiles.standard.status === 'PROCESSING') {
                        inProgressAssets++;
                    }

                    if (i === _data.length - 1) {


                        // add href if count > 0
                        if (inProgressAssets != 0) {
                            $("#in-progress-href").attr("href", "#");
                        } else {
                            $("#in-progress-href").attr("href", "#");
                        }

                        var total_assets = _data.length;
                        var total_transcoded_asset = (basicProfilesAssets + standardProfilesAssets + premiumProfilesAssets + customProfilesAssets);

                        $('#total-asset').html(total_assets);
                        $("#total-asset-graph-count").val(total_assets);
                        


                         // add href if count > 0
                         if (total_transcoded_asset != 0) {
                            $("#total-asset-href").attr("href", "#");
                        } else {
                            $("#total-asset-href").attr("href", "#");
                        }


                        $(".total-asset-graph-count").knob({
                            readOnly: true,
                            width: "80",
                            height: "80",
                            fgColor: "#f05050",
                            bgColor: "#F9B9B9",
                            skin: "tron",
                            angleOffset: "180",
                            thickness: ".15"
                        });


                        $('#total-transcoded-asset').html(total_transcoded_asset)
                        $('#total-transcoded-asset-count').val(total_transcoded_asset);

                         // add href if count > 0
                         if (total_transcoded_asset != 0) {
                            $("#total_transcoded_asset-href").attr("href", "#");
                        } else {
                            $("#total_transcoded_asset-href").attr("href", "#");
                        }

                        $(".total-transcoded-asset-count").knob({
                            readOnly: true,
                            width: "80",
                            height: "80",
                            fgColor: "#ffbd4a",
                            bgColor: "#FEE5B9",
                            skin: "tron",
                            angleOffset: "180",
                            thickness: ".15"
                        });

                        $('#in-progress').html(inProgressAssets)

                        $('#in-progress-graph-count').text(inProgressAssets + '%');

                        $('#in-progress-asset-progress-bar').css('width', inProgressAssets + "%");


                        let data = {
                            basicProfilesAssets: basicProfilesAssets,
                            standardProfilesAssets: standardProfilesAssets,
                            premiumProfilesAssets: premiumProfilesAssets,
                            customProfilesAssets: customProfilesAssets
                        }


                        createDonutChart(data)

                    }
                }
            }
        });



    // get Todays asset count
    $.get('/v1/video/todays_uploaded',
        function (data, status) {
            if (data.status == 200) {
                //console.log('todays_uploaded_assets > ', data.data)

                let todays_uploaded = data.data;
                $('#todays-uploaded-asset-count').html(todays_uploaded)
                $('#todays-uploaded-asset-graph-count').text(todays_uploaded + '%')
                $('#todays-uploaded-asset-progress-bar').css('width', todays_uploaded + "%");
                
                        // add href if count > 0
                        if (todays_uploaded != 0) {
                            $("#todays_uploaded-href").attr("href", "#");
                        } else {
                            $("#todays_uploaded-href").attr("href", "#");
                        }

            }


        });


});


function createDonutChart(data) {
    Morris.Donut({
        element: "transcode-donut-chart",
        data: [
            { label: "Basic", value: data.basicProfilesAssets },
            { label: "Standard", value: data.standardProfilesAssets },
            { label: "Premium", value: data.premiumProfilesAssets },
            { label: "Custom", value: data.customProfilesAssets },
        ],
        resize: !0,
        colors: ["#35b8e0", "#5b69bc", "#ff8acc", "#ffbd4a"],
        backgroundColor: "#323a46",
        labelColor: "#fff",
        labelFont: "12px"
    })
}