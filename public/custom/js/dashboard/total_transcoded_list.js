// get Total transcoded video count
$.post('/v1/dashboard/transcoded_list',
function (data, status) {
    if (data.status == 200) {
        console.log('total_transcoded_list_assets > ', data.data)

        let todays_uploaded = data.data;
       appendVideosDatatable(data);
    }
});



function appendVideosDatatable(data) {
    var array = data.data;
    
    
    var options_table = "";
    
    array.forEach(function (element, i) {
    
        var content_id = element.content_id ? element.content_id : "";
        var file_name = element.file_name ? element.file_name : "";
        var updated_at = element.updated_at ? moment(element.updated_at).format('lll') : "";
        var created_by = element.created_by ? element.created_by : "ADMIN";
    
    
        options_table += '<tr class="total_transcoded_list-tbl-row profile-row" id=' + content_id + '>' +
            '<td class="total_transcoded_list-title">' + file_name + '</td>' +
            '<td class="total_transcoded_list-title"> ' + updated_at + '</td>' +
            '<td class="total_transcoded_list-title"> ' + created_by + '</td>' ;
    
    
        if (i == array.length - 1) {
            $('#list_all_total_transcoded_list_tbody').append(options_table)
            reInitializeDataTable()
        }
    })
    }
    
    
    var list_all_processing_list_table;
    
    function reInitializeDataTable() {
    $("#list_all_total_transcoded_list_table").DataTable().destroy()
    list_all_profile_table = $('#list_all_total_transcoded_list_table').DataTable({
    })
    
    //initiateAssetSelection
    //initiateAssetSelection();
    }
    