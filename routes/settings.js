var express = require('express');
var router = express.Router();
const AWS = require('aws-sdk');

const Config = require("../workers/setting");
const settingObj = new Config.SettingClass();

const awsConfig = require('../config/aws_config')['development'];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});

// const mediaConvertUtil = require('../app/utilities/video/media_convert');


router.get('/', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('settings/configuration', { section: 'Settings', sub_section: 'Configuration', userData: userData, userName: userName, userRole: userRole });
});

/* POST : SAVE Configuration DETAILS. */
router.post('/', (req, res, next) => {
     //console.log("configuration POST called")
    let params = {
        name: req.body.name,
        config: req.body.config ? req.body.config : null,
    }
     //console.log("i am here>>>" + JSON.stringify(params))

    settingObj.save(params).then((_response) => {
        if (_response) {
             //console.log('Configuration data saved');
            res.json({
                status: 200,
                message: 'Saved successfuly',
                data: _response,
            })

        } else {
             //console.log('Configuration data save failed');
            res.json({
                status: 401,
                message: 'Configuration Save failed',
                data: null,
            })
        }
    })
})


// update Configuration
router.post('/updateConfig', (req, res, next) => {
     //console.log("updateConfig called")
    var params = {
        name: req.body.name
    }

     //console.log("i am here>>>" + JSON.stringify(params))

    settingObj.findOneAndUpdate(params).then((_response) => {
        if (_response) {
             //console.log('update data saved');
            res.json({
                status: 200,
                message: 'Saved successfuly',
                data: _response,
            })

        } else {
             //console.log('update data save failed');
            res.json({
                status: 401,
                message: 'update Save failed',
                data: null,
            })
        }
    })
})

module.exports = router;