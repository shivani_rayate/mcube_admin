var express = require('express');
var router = express.Router();

const Video = require('../workers/dashboard');
const dashboardObj = new Video.DashboardClass();

// let video_utility = require('../app/utilities/video');

router.get('/', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('dashboard/index', { section: 'Dashboard', sub_section: '', userData: userData, userName: userName, userRole: userRole });
});

router.get('/proccessing_list', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('dashboard/proccessing_list', { section: 'Dashboard', sub_section: 'Proccessing', userData: userData, userName: userName, userRole: userRole });
});

router.get('/todays_uploaded_list', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('dashboard/todays_uploaded_list', { section: 'Dashboard', sub_section: 'Todays Uploaded', userData: userData, userName: userName, userRole: userRole });
});


router.get('/total_transcoded_list', function (req, res, next) {
    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('dashboard/total_transcoded_list', { section: 'Dashboard', sub_section: 'Total Transcoded', userData: userData, userName: userName, userRole: userRole });
});

// get todaysuploaded_list 
router.post('/todays_uploaded', (req, res, next) => {

    const params = {

    };

    dashboardObj.countTodaysVideo(params).then((data) => {
        res.json({
            status: 200,
            message: 'Todays Videos Counted successfully',
            data: data,
        });
    });

});

// get processing_list 
router.post('/proccessing_list', (req, res, next) => {

    const params = {

        profiles: { basic: { status: "PROCESSING" } }
     };
    dashboardObj.findStatusByid(params).then((data) => {
        res.json({
            status: 200,
            message: 'List fetched successfully',
            data: data,
        });
    });
});

// get transcoded_list 
router.post('/transcoded_list', (req, res, next) => {

    const params = {


    };
    dashboardObj.findlistBystatus(params).then((data) => {
        res.json({
            status: 200,
            message: 'List of transcoded videos fetched successfully',
            data: data,
        });
    });
});

module.exports = router;