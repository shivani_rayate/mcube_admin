const express = require('express');
const router = express.Router();

const AWS = require('aws-sdk');
const cognito = require('../app/utilities/tenant_management/cognito');
const mongodb = require('../app/utilities/mongodb/create');
const awsConfig = require('../config/aws_config')['development'];
const tenantConfig = require('../config/tenantConfig');


AWS.config.update({
  accessKeyId: awsConfig.accessKeyId,
  secretAccessKey: awsConfig.secretAccessKey,
  region: awsConfig.region,
});

router.get('/tenants', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('admin/tenants', { section: 'Tenant Management', sub_section: 'Tenants', userData: userData, userName: userName, userRole: userRole });

})


router.get('/create_tenant', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('admin/create_tenant', { section: 'Tenant Management', sub_section: 'Create Tenant', userData: userData, userName: userName, userRole: userRole });
})


router.get('/tenantInfo', function (req, res, next) {

  const userData = req.session.userData ? req.session.userData : null;
  const userName = req.session.userName ? req.session.userName : null;
  const userRole = req.session.userRole ? req.session.userRole : null;
  res.render('admin/tenantInfo', { section: 'Tenant Management', sub_section: 'Tenant Information', userData: userData, userName: userName, userRole: userRole });
})



function mcubeTranscodeClouformation(params) {
  return new Promise((resolve, reject) => {
     console.log("\n \n \n mcubeTranscodeClouformation called >> ")

    let _params = {
      FunctionName: awsConfig.cloudformation.mcube_transcode,
      Payload: JSON.stringify(params),
    };
    console.log("\n \n \n mcubeTranscodeClouformation called  _params >> " +JSON.stringify(_params))

    const mcubeTranscodeConfig = (new AWS.Lambda().invoke(_params).promise());
    resolve(mcubeTranscodeConfig)
  })
}


function mcubeAdvTranscodeClouformation(params) {
  return new Promise((resolve, reject) => {
    // console.log("\n \n \n mcubeAdvTranscodeClouformation called >> ")

    let _params = {
      FunctionName: awsConfig.cloudformation.mcube_adv_transcode,
      Payload: JSON.stringify(params),
    };
    const mcubeAdvTranscodeConfig = (new AWS.Lambda().invoke(_params).promise());
    resolve(mcubeAdvTranscodeConfig)
  })
}


function mcubeDataScienceClouformation(params) {
  return new Promise((resolve, reject) => {
    // console.log("\n \n \n mcubeDataScienceClouformation called >> ")

    let _params = {
      FunctionName: awsConfig.cloudformation.mcube_data_science,
      Payload: JSON.stringify(params),
    };
    const mcubeDataScienceConfig = (new AWS.Lambda().invoke(_params).promise());
    resolve(mcubeDataScienceConfig)
  })
}


router.post('/createUserPoolApp', (req, res, next) => {
  //  console.log("/n create user pool app POST called > ")
  const params = {
    poolName: req.body.poolName ? req.body.poolName : null,
    mcube_adv_transcode: req.body.modules.mcube_adv_transcode,
    mcube_data_science: req.body.modules.mcube_data_science
  }
  console.log('create user pool >> >>' +JSON.stringify(params))


  // check for duplicate tenant name in dynamodb
  var docClient = new AWS.DynamoDB.DocumentClient();
  var _params = {
    TableName: awsConfig.cloudformation.configDynamodbTable,
    Key: {
      'poolName': params.poolName
    }
  };

  console.log('create user pool11 >> >> >>' +JSON.stringify(_params))
  const getTenantPromise = docClient.get(_params).promise();
  console.log(' getTenantPromise >> >> >>' +JSON.stringify(getTenantPromise))
  getTenantPromise.then(
   (data) => {

      console.log('In get tenant >> 12345 >> >> >>' +JSON.stringify(data))

      if (data.Item && data.Item.userPool.UserPool.Name === params.poolName){
        res.json({
          status: 409, // status code for conflict
          message: 'Tenant already exists! Please use a different Tenant Name',
          data: null,
        });
      } else {

        cognito.createUserPoolApp(params).then((response) => {

          console.log("\n create user pool response >>")
          if (response){

             console.log("\n create user pool response >> " + JSON.stringify(response))
            // infra creation by lambda
            let _params = {
              poolName: req.body.poolName ? req.body.poolName : null,
              clientId: response.userPoolClientData.UserPoolClient.ClientId
            }

            console.log('IN create user pool  >> '+JSON.stringify(_params))

            const promise1 = mcubeTranscodeClouformation(_params);
            const promise2 = (params.mcube_adv_transcode === 'true') ? mcubeAdvTranscodeClouformation(_params) : null;
            const promise3 = (params.mcube_data_science === 'true') ? mcubeDataScienceClouformation(_params) : null;

            Promise.all([promise1, promise2, promise3]).then(function (clouformationResponse) {

              // console.log('\n Success! infra creation by lambda >> \n ');

              let userPoolData = response.userPoolData;
              let userPoolClientData = response.userPoolClientData;

              var infra_config_params = {
                TableName: awsConfig.cloudformation.configDynamodbTable,
                Item: {
                  "poolId": userPoolData.UserPool.Id,
                  "poolName": userPoolData.UserPool.Name,
                  "clientId": userPoolClientData.UserPoolClient.ClientId,
                  "userPool": userPoolData,
                  "userPoolClient": userPoolClientData,
                  "mongodb": response.mongodb,
                  "config": {
                    "dynamodbTables": {
                      "mcube_transcode": {},
                      "mcube_adv_transcode": {},
                      "mcube_data_science": {}
                    },
                    "s3BucketList": {}
                  }
                }
              };
              console.log('IN lambda infra >> >> >> '+JSON.stringify(infra_config_params))


              if (clouformationResponse[0] !== null && clouformationResponse[0].StatusCode === 200) {
                var _mcubeTranscodeConfig = JSON.parse(clouformationResponse[0].Payload);
                infra_config_params.Item.config.dynamodbTables.mcube_transcode = {
                  "previewTbl": _mcubeTranscodeConfig.data.dynamoDb.previewDbName,
                  "basicProfileTbl": _mcubeTranscodeConfig.data.dynamoDb.basicDbName,
                  "standardProfileTbl": _mcubeTranscodeConfig.data.dynamoDb.standardDbName,
                  "premiumProfileTbl": _mcubeTranscodeConfig.data.dynamoDb.premiumDbName,
                  "customProfileTbl": _mcubeTranscodeConfig.data.dynamoDb.customDbName,
                  "mediainfoTbl": _mcubeTranscodeConfig.data.dynamoDb.mediaInfoDbName,
                }
                infra_config_params.Item.config.s3BucketList = {
                  "inputBucket": _mcubeTranscodeConfig.data.S3BucketList.inputBucket,
                  "outputBucket": _mcubeTranscodeConfig.data.S3BucketList.outputBucket
                }
              }

              if (promise2 !== null && clouformationResponse[1].StatusCode === 200) {
                var _mcubeAdvTranscodeConfig = JSON.parse(clouformationResponse[1].Payload);
                infra_config_params.Item.config.dynamodbTables.mcube_adv_transcode = {
                  "advtranscodeDbName": _mcubeAdvTranscodeConfig.data.dynamoDb.advtranscodeDbName,
                }
              }

              if (promise3 !== null && clouformationResponse[2].StatusCode === 200) {
                var _mcubeDataScienceConfig = JSON.parse(clouformationResponse[2].Payload);
                infra_config_params.Item.config.dynamodbTables.mcube_data_science = {
                  "recognizeDbName": _mcubeDataScienceConfig.data.dynamoDb.recognizeDbName,
                  "videoindexDbName": _mcubeDataScienceConfig.data.dynamoDb.videoindexDbName,
                }
              }

              //console.log("\n infra congfig params ITEM \n>> " + JSON.stringify(infra_config_params.Item))

              //save tenant config to dynamodb
              docClient.put(infra_config_params, function (err, data) {
                if (err) {
                  console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
                } else {
                  res.json({
                    status: 200,
                    message: 'create user pool Successfully',
                    data: response,
                    config: infra_config_params.Item
                  })
                }
              });
            })
          }
        }).catch((err) => {
          console.log(`error IN create user pool: ${err}`);
          res.json({
            status: 500,
            message: 'Oops ! Some error occured, please try again later.',
            data: null,
          });
        });

      }
    },
    (err) => {
      console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
      res.json({
        status: 500,
        message: 'Oops ! Some error occured, please try again later.',
        data: null,
      });
    }
  );
})


//  list user pool
router.get('/listUserPools', (req, res, next) => {
  // console.log("list user pool POST called")

  const params = {
    maxResults: req.query.maxResults ? req.query.maxResults : null
  }

  cognito.listUserPools(params).then((response) => {
    if (response) {
      res.json({
        status: 200,
        message: 'list user pool Successfully',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

// Describe user pool data
router.get('/describeUserPool', (req, res, next) => {
  //  console.log("Describe user pool Get called")

  const params = {
    userPoolId: req.query.userPoolId ? req.query.userPoolId : null
  }
  cognito.describeUserPool(params).then((response) => {
    if (response) {
      //console.log(' Describeuser pool  response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'Describe user pool Successfully',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

// create mongo DB
router.post('/createMongodb', (req, res, next) => {
  // console.log("create mongodb Post called ");
  // const params = {
  //   name: req.body.name ? req.body.name: null,
  //   stackName: req.body.stackName ? req.body.stackName: null,
  //   tenant: req.body.tenant ? req.body.tenant : null,
  // }
  //console.log(JSON.stringify(params))
  createMongodbObj.createMongodb().then((response) => {
    if (response) {
      res.json({
        status: 200,
        message: "Create Mongodb Successfully",
        data: response
      })

    } else {
      res.json({
        status: 400,
        message: "failed",
        data: null
      })
    }
  });

})

// register tenant admin
router.post('/register', (req, res, next) => {
  // console.log("register POST called")
  const params = {
    userPoolId: req.body.userPoolId ? req.body.userPoolId : null,
    name: req.body.name ? req.body.name : null,
    email: req.body.email ? req.body.email : null,
    password: req.body.password ? req.body.password : null
  };
  cognito.RegisterUser(params).then((response) => {
    if (response) {
      // console.log('user data saved response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Saved Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER SAVE: ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });

});

// Delete Tenant Info
router.post('/deleteTenant', (req, res, next)=>{

  // console.log("deleteTenant called in admin")
    var tenantId = req.body.userPoolId;
    console.log("deleteTenant called in admin >> >> " +JSON.stringify(tenantId))

  const params = {
    clientId: tenantConfig[tenantId].config.clientId,
    userPoolId: req.body.userPoolId ? req.body.userPoolId : null,
    poolName: req.body.poolName ? req.body.poolName : null
  };
   console.log("deleteTenant called in admin" +JSON.stringify(params))


  cognito.deleteUserPool(params).then((response) => {
    if (response) {
      // console.log("\n \n \n \n + callback from delete cognito")
      const main = async ()=>{
        const _params = {
          FunctionName: awsConfig.cloudformation.deleteStackLambda,
          Payload: JSON.stringify(params),
        };
        const deleteInfraStack = await (new AWS.Lambda().invoke(_params).promise());
        // console.log('\n Success! infra deletion by lambda >> \n ');


        //delete tenant config from dynamodb
        let _paramsDynamodb = {

          TableName: awsConfig.cloudformation.configDynamodbTable,
          Key: {
            poolName: req.body.poolName ? req.body.poolName : null
          }
        }

        var docClient = new AWS.DynamoDB.DocumentClient();

        docClient.delete(_paramsDynamodb, function (err, data) {
          if (err) {
            console.error("Unable to delete item. Error JSON:", JSON.stringify(err, null, 2));
          } else {
            console.log("DeleteItem succeeded:", JSON.stringify(data, null, 2));
            res.json({
              status: 200,
              message: 'Tenant Deleted Successfully',
              data: null,
            })

          }
        });
      };
      main().catch(error => {
        console.error(error)
        res.json({
          status: 500,
          message: 'Tenant infra Delete failed',
          data: null,
        })
      })

    }

  });

});

// Get List Users In Group
router.get('/listAdminInTenant', (req, res, next) => {
  // console.log('***GET listAdminInTenant CALLED***');

  const params = {
    userPoolId: req.query.userPoolId ? req.query.userPoolId : null
  }
  cognito.listUsersInGroup(params).then((users) => {
    res.json({
      status: 200,
      message: 'List Of Users in Group fetched successfully',
      data: users,
    });
  });
});

module.exports = router;