const express = require('express');
const router = express.Router();
var fs = require("fs");
const tenantConfig = require('../config/tenantConfig');

// ##############################################################################

let requestMethods = require("../app/utilities/commonRequestMethods.js");
let API = require('../config/api/apiList');

// ########## MICRO-ui ##########
let mcube_ui_BASE = API.mcube_ui.base.development;
let mcube_transcode_BASE = API.mcube_transcode.base.development;
let mcube_adv_transcode_BASE = API.mcube_adv_transcode.base.development;
let mcube_cognito_BASE = API.mcube_cognito.base.development;

function updateConfig(params){
  return new Promise((resolve, reject) => {
    requestMethods.postApiRequest(params).then((response) => {
      resolve(response);
    }).catch((err) => {
      console.log(`error IN config SAVE: ${err}`);
      reject(err)
    });
  })
}
function deleteConfig(params){
  return new Promise((resolve, reject) =>{
    requestMethods.postApiRequest(params).then((response) =>{
      resolve(response);
    }).catch((err) => {
      console.log(`error IN config SAVE: ${err}`);
      reject(err)
    });
  })
}

router.post('/addTenantConfig', (req, res, next) => {
  //console.log(" update config  POST called in admin")

  var config = req.body.config;
  var tenantName = req.body.config.poolName;
  var tenantId = req.body.config.poolId;

  tenantConfig[tenantId] = { tenantName:tenantName, config };
  fs.writeFile('config/tenantConfig.json', JSON.stringify(tenantConfig, null, 2), (err) => {
    if (err) {
      console.error(err);
      return;
    };
    //console.log("File has been created");
  });

// Update tenant config in all microservices
   let mcubeUi = {
    url: mcube_ui_BASE + API.mcube_ui.config.addTenantConfig,
    data: {
      config: config
    }
  }
  const promise1 = updateConfig(mcubeUi);

  let mcubeTranscode = {
    url: mcube_transcode_BASE + API.mcube_transcode.config.addTenantConfig,
    data: {
      config: config
    }
  }
  const promise2 = updateConfig(mcubeTranscode);


  let mcubeAdvTranscode = {
    url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.config.addTenantConfig,
    data: {
      config: config
    }
  }
  const promise3 = updateConfig(mcubeAdvTranscode);


  let mcubeCognito = {
    url: mcube_cognito_BASE + API.mcube_cognito.config.addTenantConfig,
    data: {
      config: config
    }
  }
  const promise4 = updateConfig(mcubeCognito);

  Promise.all([promise1, promise2, promise3, promise4]).then(function (response) {
  console.log(response);
  res.json({
      status: 200,
      message: 'Tenant config updated in all microservices.',
      data: null,
    });

  })

})

// delete tenant config from config.json file
router.post('/deleteTenantConfig', (req, res, next) => {
 //console.log("deleteTenantConfig called in API");

  var params = {
    userPoolId: req.body.userPoolId ? req.body.userPoolId : null,
    poolName: req.body.poolName ? req.body.poolName : null
  }
  delete tenantConfig[params.userPoolId];
  fs.writeFile('config/tenantConfig.json', JSON.stringify(tenantConfig, null, 2), (err) => {
    if (err) {
      console.error(err);
      return;
    };
    //console.log("File has been deleted");
  });


  // delete tenant config in all microservices

  let mcubeUi = {
    url: mcube_ui_BASE + API.mcube_ui.config.deleteTenantConfig,
    data: params
  }
  const promise1 = deleteConfig(mcubeUi);

  let mcubeTranscode = {
    url: mcube_transcode_BASE + API.mcube_transcode.config.deleteTenantConfig,
    data: params
  }
  const promise2 = deleteConfig(mcubeTranscode);


  let mcubeAdvTranscode = {
    url: mcube_adv_transcode_BASE + API.mcube_adv_transcode.config.deleteTenantConfig,
    data: params
  }
  const promise3 = deleteConfig(mcubeAdvTranscode);

  let mcubeCognito = {
    url: mcube_cognito_BASE + API.mcube_cognito.config.deleteTenantConfig,
    data: params
  }
  const promise4 = deleteConfig(mcubeCognito);

  Promise.all([promise1, promise2, promise3, promise4]).then(function (response) {
    console.log(response);

    res.json({
      status: 200,
      message: 'Tenant config delete in all microservices.',
      data: null,
    });
  })
})

module.exports = router;
