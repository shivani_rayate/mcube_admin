const express = require('express');
const router = express.Router();

//const Users = require("../workers/users");
//const usersObj = new Users.UserClass();

const cognito = require('../app/utilities/user_management/cognito');

const download = require('download');
const fs = require('fs');


// const Notification = require("../workers/notification");
// const notificationObj = new Notification.NotificationClass();

const createMongodbObj = require('../app/utilities/mongodb/create');

router.get('/', function (req, res, next) {
  let user_id = req.session ? (req.session.user_id ? req.session.user_id : null) : null;
  if (user_id) {
    res.redirect('/v1/video')
  } else {
    res.render('login');
  }
});


router.get('/lockscreen', function (req, res, next) {
  res.render('other/lockscreen');
});

router.get('/page_404', function (req, res, next) {
  res.render('other/page_404');
});

router.get('/page_500', function (req, res, next) {
  res.render('other/page_500');
});

router.get('/temp', function (req, res, next) {
  res.render('temp');
});


router.get('/authenticateUser', function (req, res, next) {
  res.render('user/authenticateUser');
});

router.get('/forgotPassword', function (req, res, next) {
  res.render('user/forgotPassword');
});



// Login
router.post('/login', (req, res, next) => {
  //console.log("LOGIN POST called")

  let params = {
    username: req.body.username,
    password: req.body.password,
  }

  cognito.Login(params).then((response) => {
    if (response.status === 200) {
      // console.log('user sign in response >> ' + JSON.stringify(response));
      req.session.user_id = "ADMIN";
      //set user details in sesssion
      req.session.userData = response.data;
      req.session.userName = response.data.idToken.payload.email;
      req.session.userRole = (response.data.idToken.payload['cognito:groups']) ? (response.data.idToken.payload['cognito:groups']) : 'User';

      //console.log("USER Info >>>>>   ")
      //console.log("userData>>>>>>>>>>>>>>>>>>>>   " + JSON.stringify(req.session.userData))
      //console.log("userName>>>>>>>>>>>>>>>>>>>>   " + req.session.userName)
      // console.log("userRole>>>>>>>>>>>>>>>>>>>>  " + req.session.userRole)
      res.json({
        status: 200,
        message: 'Login successful'
      });
    } else {
      res.json({
        status: 403,
        message: response.message,
        data: response.data,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER sign in : ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})

// authenticateUser with AWS Cognito
router.post('/authenticateUser', (req, res, next) => {
  //console.log("authenticateUser with AWS Cognito called");

  let params = {
    username: req.body.username,
    password: req.body.password,
    newPassword: req.body.newPassword
  }
  cognito.AuthenticateUser(params).then((response) => {
    if (response.status === 200) {
      // console.log('user sign in response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Signed Successfuly',
        data: response.data,
      })
    } else {
      res.json({
        status: 403,
        message: response.message,
        data: response.data,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER sign in : ${err}`);
    res.json({
      status: 500,
      message: 'Oops ! Some error occured, please try again later.',
      data: null,
    });
  });
})


// Forgot Password
router.post('/forgotPassword', (req, res, next) => {
  console.log("forgotPassword API called");

  let params = {
    username: req.body.username
  }

  cognito.ForgotPassword(params).then((response) => {
    console.log('ForgotPassword response >> ' + JSON.stringify(response));

    res.json({
      status: 200,
      message: 'ForgotPassword Success',
      data: response
    });

  }).catch((err) => {
    console.log(`error IN ForgotPassword : ${err}`);
    res.json({
      status: 500,
      message: err.message,
      data: null,
    });
  });
})


// Reset Password
router.post('/resetPassword', (req, res, next) => {
  console.log("resetPassword API called");

  let params = {
    ConfirmationCode: req.body.verificationCode,
    Password: req.body.password,
    Username: req.body.username
  }

  cognito.ConfirmForgotPassword(params).then((response) => {
    console.log('resetPassword response >> ' + JSON.stringify(response));

    res.json({
      status: 200,
      message: 'resetPassword Success',
      data: response
    });

  }).catch((err) => {
    console.log(`error IN resetPassword : ${err}`);
    res.json({
      status: 500,
      message: err.message,
      data: null,
    });
  });
})



//Logout
router.get('/logout', function (req, res) {

  req.session.destroy(function (err) {
    if (err) {
      res.redirect('/');
    } else {
      req.session = null;
      console.log("Logout Success " + JSON.stringify(req.session) + " ");
      res.redirect('/login');
    }
  });
});


router.get('/404', function (req, res, next) {
  console.log('BEFORE')

  setTimeout(() => {
    //console.log('END OF SET TIMEOUT')
    res.render('404', { title: '404 Error' });
  }, 120000);
});


// Register
router.get('/register', function (req, res, next) {
  res.render('user/register');
});


// registration
router.post('/register', (req, res, next) => {

  //console.log("register POST called")

  const params = {
    name: req.body.name ? req.body.name : null,
    email: req.body.email ? req.body.email : null,
    password: req.body.password ? req.body.password : null
  };

  cognito.RegisterUser(params).then((response) => {
    if (response) {
      //console.log('user data saved response >> ' + JSON.stringify(response));
      res.json({
        status: 200,
        message: 'User Saved Successfuly',
        data: response,
      })
    }
  }).catch((err) => {
    console.log(`error IN USER SAVE: ${err}`);
    res.json({
      status: 500,
      message: err.message,
      data: null,
    });
  });

});

/////////////////////////////////////////////////////////////////

router.get('/downloadFile', (req, res) => {

  //console.log('Download called >>');

  var url = req.query.url;
  var options = {
    filename: 'package.mp4'
  }

  download(url, 'download', options).then((data) => { // (url, directory, options)

    res.download('download/package.mp4', function (err) {
      if (err) throw err
      // console.log("Downloaded!")
      fs.unlink('download/package.mp4', function (err) {
        if (err) throw err;
        // console.log('File deleted!');

      });
    })
  })

})

// save notification 
router.post('/notification', (req, res, next) => {
  //console.log("Notification POST called")

  const params = {
    text: req.body.text ? req.body.text : null,
    action: req.body.action ? req.body.action : null,
    time: req.body.time ? req.body.time : null,
  }
  //console.log("notification data > " + JSON.stringify(params))

  notificationObj.save(params).then((_response) => {
    if (_response) {
      //  console.log('Notification data saved');
      res.json({
        status: 200,
        message: 'Notification Saved successfuly',
        data: _response,
      })

    } else {
      //console.log('Notification data save failed');
      res.json({
        status: 401,
        message: 'Notification Save failed',
        data: null,
      })
    }
  })


})

router.get('/notification', (req, res, next) => {
  //console.log("get notification data");

  notificationObj.findAll().then((Notification) => {
    res.json({
      status: 200,
      message: 'notification successfully',
      data: Notification,
    })

  })
})

router.post('/removeNotification', (req, res, next) => {
  // console.log("remove notifications Post called ");
  const params = {
    text: req.body.text ? req.body.text : null,
    action: req.body.action ? req.body.action : null,
    time: req.body.time ? req.body.time : null,
  }
  // console.log(JSON.stringify(params))
  notificationObj.removeNotification(params).then((response) => {
    if (response) {
      res.json({
        status: 200,
        message: "Remove Notifications successfully",
        data: response
      })

    } else {
      res.json({
        status: 400,
        message: "failed",
        data: null
      })
    }
  });
})


// save
router.post('/createMongodb', (req, res, next) => {
  //console.log("create mongodb Post called ");
  // const params = {
  //   name: req.body.name ? req.body.name: null,
  //   stackName: req.body.stackName ? req.body.stackName: null,
  //   tenant: req.body.tenant ? req.body.tenant : null,
  // }
  //console.log(JSON.stringify(params))
  createMongodbObj.createMongodb(params).then((response) => {
    if (response) {
      res.json({
        status: 200,
        message: "Create Mongodb Successfully",
        data: response
      })

    } else {
      res.json({
        status: 400,
        message: "failed",
        data: null
      })
    }
  });

})


module.exports = router;