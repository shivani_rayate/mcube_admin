const express = require('express');
const router = express.Router();

const AWS = require('aws-sdk');

const multer = require('multer');

const multerS3 = require('multer-s3');

const videoPartial = require('../app/utilities/video/index');

const mediaConvertUtil = require('../app/utilities/video/media_convert');

const uuidv1 = require('uuid/v1');

const Video = require('../workers/video');
const videoObj = new Video.VideoClass();



// const Profiles = require("../workers/profiles");
// const profilesObj = new Profiles.ProfilesClass();

let video_utility = require('../app/utilities/video');

const awsConfig = require('../config/aws_config')['development'];
const tenantConfig = require('../config/tenantConfig');

var moment = require('moment');


AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});
const s3 = new AWS.S3();

const listS3 = require('../app/utilities/listS3');


router.get('/fetch_all_videos', (req, res, next) => {

    //console.log('***GET ALL VIDEOS CALLED***');

    videoObj.findall().then((videos) => {
        res.json({
            status: 200,
            message: 'Videos fetched successfully',
            data: videos,
        });
    }).catch((err) => {
        console.log(`error IN Video fetched : ${err}`);
        res.json({
            status: 500,
            message: 'Oops ! Some error occured, please try again later.',
            data: null,
        });
    });
})


router.get('/listS3_Assets/:tenantId', function (req, res, next) {

    // console.log('***GET ALL listS3_Assets CALLED in API***');

    let tenantId = req.params.tenantId;

    var params = {
        bucket: tenantConfig[tenantId].config.config.s3BucketList.inputBucket
    }
    listS3.listS3Assets(params).then((videos) => {

        res.json({
            status: 200,
            message: 'listS3_Assets fetched successfully',
            data: videos,
        });
    });

})


// create preview of S3 List Asset
router.post('/create-preview', (req, res, next) => {

    const params = {
        content_id: req.body.content_id,
    };

    listS3.create_preview_asset(params).then(() => {
        res.json({
            status: 200,
            message: 'Preview creation is in Progress!',
            data: null,
        });
    });

});


// getlink preview asset
router.post('/getlink-preview', (req, res, next) => {
    //console.log("getlink preview  POST CALLED in API")
    let params = req.body;
    listS3.getlink_preview_asset(params).then((url) => {
        if (res) {
            res.json({
                status: 200,
                message: "getlink Preview asset successfully",
                data: url
            })
        } else {
            res.json({
                status: 400,
                message: "getlink failed",
                data: null
            })
        }
    })

});


router.get('/listS3_PreviewAssets', function (req, res, next) {

    //console.log('***GET ALL listS3_PreviewAssets CALLED in API***');

    let tenantId = res.locals.tenantId;
    var params = {
        dynamoDB_table: `${tenantConfig[tenantId].config.clientId}-adv-transcode-db`
    }

    listS3.listS3AssetsDynamodb().then((videos) => {
        res.json({
            status: 200,
            message: 'listS3_PreviewAssets fetched successfully',
            data: videos,
        });
    });

})


// meta data
router.post('/listS3ContentMetadatatDynamodb', function (req, res, next) {

    const params = {
        assetid: req.body.asset_id
    };
    listS3.listS3ContentMetadatatDynamodb(params).then((data) => {
        res.json({
            status: 200,
            message: 'ContentMetadatatDynamodb fetched successfully',
            data: data,
        });
    });

})


// upload audio files to S3
router.post('/uploadAudio/:params', (req, res, next) => {

    // console.log("audio upload route called");

    var _params = JSON.parse(req.params.params)

    let asset_id = _params.asset_id;
    let language = _params.language;

    // for uploading audio files
    var audioFileNameArr = [];
    var _i = 0;

    var upload = multer({

        storage: multerS3({

            s3: s3,
            bucket: `${awsConfig.bucket}/${awsConfig.DISNEY_CONFIG.previewFolder}/${asset_id}`,
            acl: 'public-read',
            contentType: multerS3.AUTO_CONTENT_TYPE,

            key: function (req, file, cb) {

                //console.log(file);

                var extension = (file.originalname).split('.').pop();
                var audioFileName = `${asset_id}_${language[_i]}.${extension}`;
                if (file.fieldname == 'files') {
                    audioFileNameArr.push({
                        "langauge": language[_i],
                        "fileName": audioFileName
                    })
                }
                cb(null, audioFileName);
                _i++;
            }
        })

    }).fields([{
        name: 'files', maxCount: 10
    }]);
    // audio file upload ends here

    upload(req, res, (error) => {
        if (error) {
            console.log(`AUDIO UPLOAD ERROR: ${error}`);
        } else {
            //console.log("upload audio success")
            //console.log("audioFileNameArr " + JSON.stringify(audioFileNameArr))
            res.json({
                status: 200,
                data: audioFileNameArr,
                message: 'Audio Files Uploaded Successfully!',
            });

            // to update dynamodb
            let _params = {
                assetid: asset_id,
                audio_info: audioFileNameArr,
            };
            listS3.updateDynamodb(_params);

        }
    })

})


// transcode-asset-disney
router.post('/transcode-asset-disney', function (req, res, next) {
    //console.log("transcode-asset-disney route called");

    let params = req.body;
    listS3.transcode_asset(params).then((response) => {
        if (response) {
            res.json({
                status: 200,
                message: "Asset Transcode is in Progress",
                data: null
            })
        } else {
            res.json({
                status: 400,
                message: "Asset Transcode failed",
                data: null
            })
        }
    })
})


// create meta data AZURE
router.post('/create-meta-data-azure', function (req, res, next) {

    //console.log("create-meta-data-azure route called in API");

    let params = req.body;

    listS3.create_meta_data_azure(params).then((response) => {

        if (response) {

            res.json({
                status: 200,
                message: "Meta Data fetch is in Progress",
                data: null
            })
        } else {
            res.json({
                status: 400,
                message: "Meta Data fetch failed",
                data: null
            })
        }
    })
})


// get meta data AZURE
router.post('/get-meta-data-azure', function (req, res, next) {

    // console.log("get-meta-data-azure route called in API");

    let params = req.body;

    listS3.get_meta_data_azur(params).then((response) => {

        if (response) {

            res.json({
                status: 200,
                message: "Azure Meta Data fetch is in Progress",
                data: response
            })
        } else {
            res.json({
                status: 400,
                message: "Azure Meta Data fetch failed",
                data: null
            })
        }
    })
})


// Get meta data Imdb
const imdb = require('../app/utilities/Metadata/imdb');
router.post('/getImdbData', function (req, res, next) {

    //console.log("getImdbData route called");

    let params = {
        assetid: req.body.asset_id,
        name: req.body.name,
    }

    imdb.getImdbData(params).then((response) => {
        if (response.status === 200) {
            res.json({
                status: 200,
                message: "Imdb Meta Data fetched",
                data: response.data
            })
        } else {
            res.json({
                status: 400,
                message: "Imdb Meta Data not found",
                data: null
            })
        }
    })
})


//update-IMD metadata status into dynamoDB
router.post('/updateImdbStatus', function (req, res, next) {

    // console.log("updateImdbStatus route called in API");

    let params = {
        assetid: req.body.asset_id,
        imdb_data_status: req.body.imdb_data_status,
    }

    imdb.updateImdbStatus(params).then((response) => {
        if (response) {

            res.json({
                status: 200,
                message: "updateImdbStatus is in Progress",
                data: response
            })
        }

    }).catch((err) => {
        //console.log("Exception updateImdbStatus >> " + JSON.stringify(err))
        res.json({
            status: 400,
            message: "updateImdbStatus failed",
            data: null
        })
    });
})


// find Imdb data by Id
router.get('/findMetaData-imdb/:asset_id', function (req, res, next) {
    //console.log("findMetaData-imdb route called");

    let asset_id = req.params.asset_id;
    videoObj.findDataByidimdb(asset_id).then((response) => {

        if (response) {
            res.json({
                status: 200,
                message: "Imdb Meta Data Fetched Successfully",
                data: response
            })
        } else {
            res.json({
                status: 400,
                message: "Imdb Meta Data Fetched failed",
                data: null
            })
        }
    })
})


// #################################################################################
const media_convert = require('../app/utilities/asset_management/advanced_transcoding/media_convert');

// CHECK MEDIA CONVERTER STATUS WITH JOBID
router.get('/getMediaConvertStatus/:job_id', function (req, res, next) {
    //console.log("getMediaConvertStatus route called in API");

    let job_id = req.params.job_id;
    media_convert.getMediaConvertStatus(job_id).then((response) => {
        if (response) {
            res.json({
                status: response.status,
                data: response.data,
                message: response.message
            })
        } else {
            res.json({
                status: 400,
                message: "getMediaConvertStatus Fetched failed",
                data: null
            })
        }
    })
})

module.exports = router;