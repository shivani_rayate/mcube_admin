var express = require('express');
var router = express.Router();
var fx = require("money");

// node-mailer.js
const nodeMailer = require('nodemailer');
const path = require('path');


const AWS = require('aws-sdk');
const awsConfig = require('../config/aws_config')['development'];
const tenantConfig = require('../config/tenantConfig');

const Video1 = require('../workers/video');
const videoObj = new Video1.VideoClass();

const Video = require('../workers/dashboard');
const dashboardObj = new Video.DashboardClass();


AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});
const dataTransfer = require('../app/utilities/invoice');


router.get('/', function (req, res, next) {

    const userData = req.session.userData ? req.session.userData : null;
    const userName = req.session.userName ? req.session.userName : null;
    const userRole = req.session.userRole ? req.session.userRole : null;
    res.render('invoice/index', { section: 'Create Report', sub_section: '', userData: userData, userName: userName, userRole: userRole });

})

function getTranscodedAssets(tableName) {
    return new Promise((resolve, reject) => {

        var params = {
            TableName: tableName
        };

        var docClient = new AWS.DynamoDB.DocumentClient();
        docClient.scan(params, onScan);
        function onScan(err, data) {
            if (err) {
                  //console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
                reject(err)
            } else {
                  //console.log(`\n \n ${tableName} >> Scan succeeded. ${JSON.stringify(data)}`);
                resolve(data)
            }
        }
    })
}


router.post('/dataTransferBilling', (req, res, next) => {
     console.log(" In data transfer billing")
     const params1 = {
       url : ` HTTP/1.1 Host:Bucket.s3.amazonaws.com`
    };
    console.log(" In route Id >> " +JSON.stringify(params1))
    dataTransfer.data_transfer(params1).then(() =>{
        res.json({
            status: 200,
            message: 'data transfer success!',
            data: null,
        });
    });

});

router.post('/getProfiles', (req, res, next) => {
    // console.log("getProfiles post called in routes >> ")

    const params = {
        startDate: req.body.startDate ? req.body.startDate : null,
        endDate: req.body.endDate ? req.body.endDate : null
    }

    let tenantId = req.body.tenantId

    let previewTbl = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.previewTbl
    let basicProfileTbl = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.basicProfileTbl
    let standardProfileTbl = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.standardProfileTbl
    let premiumProfileTbl = tenantConfig[tenantId].config.config.dynamodbTables.mcube_transcode.premiumProfileTbl

    const promise1 = getTranscodedAssets(previewTbl);
    const promise2 = getTranscodedAssets(basicProfileTbl);
    const promise3 = getTranscodedAssets(standardProfileTbl);
    const promise4 = getTranscodedAssets(premiumProfileTbl);

    Promise.all([promise1, promise2, promise3, promise4]).then(function (response) {
        //console.log(`\n\n\n\n\n\ response  >> ${JSON.stringify(response)}`);

        res.json({
            status: 200,
            message: 'getProfiles success.',
            data: response,
        });

    })

})

let requestMethods = require("../app/utilities/commonRequestMethods.js");
router.get('/exchangeratesapi', (req, res, next) => {

    let params = {
        url: `https://api.exchangeratesapi.io/latest`
    }

    requestMethods.getApiRequest(params).then((exchangerates) => {
        res.json(exchangerates);
    });
})

//billing email send 
//router.post('/emailSend', (req, res, next) => {
    //console.log("In email Send >>> >> >> >>")

    //const params = {
      //  tenantName: req.body.tenantName ? req.body.tenantName : null,
       // pdf: req.body.pdf ? req.body.pdf : null,
    //}

    //let transporter = nodeMailer.createTransport({
       // service: 'gmail',
       // auth:{
            // should be replaced with real sender's account
          //  user: 'gavandesuvarna63@gmail.com',
           // pass: 'suvarna1997'
       // }
    //});
    //let mailOptions = {
        //should be replaced with real recipient's account
       // from: 'gavandesuvarna63@gmail.com',
       // to: 'sgavande@skandha.in',
        //subject: 'Billing Report ',
      //  text: `${params.tenantName}`,
       // attachments: [
            //{
               //filename:`C:/Users/DELL/Downloads/${params.tenantName}`,
              //  contentType: 'application/pdf'
           // }]
    //};
    //transporter.sendMail(mailOptions, (error, info) => {
        //if (error) {
          //  return console.log('mail error ' + error);
      //  } else {
        //    console.log('Mail send successfully');
        //}
    //});
//})
module.exports = router;