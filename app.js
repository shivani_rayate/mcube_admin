const express = require('express');
const router = express.Router();
const createError = require('http-errors');
const path = require('path');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const mongoose = require('mongoose');
const compression = require('compression');
const chalk = require('chalk');
const cors = require('cors');
const morgan = require('morgan');

// ****************** SETTING GLOBAL ENV ******************
global.ENV = process.env.NODE_ENV || 'development';
console.log('APP JS ENVIRONMENT: ', ENV);


const index = require('./routes/index');
const users = require('./routes/users');
const video = require('./routes/video');
const dashboard = require('./routes/dashboard');
// const profile = require('./routes/profile');
const settings = require('./routes/settings');
// const imdb = require('./routes/testimdb');
const admin = require('./routes/admin');
const invoice = require('./routes/invoice');
const config = require('./routes/config');

const app = express();

// Logging
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}

/**
 * Express configuration.
 */
app.set('port', process.env.PORT || 3004);
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// CORS - To hanlde cross origin requests
app.use(cors());

//compress all responses to Make App Faster
app.use(compression());


app.use(express.json({ limit: '26000mb' }));
app.use(express.urlencoded({ // to support URL-encoded bodies
  limit: '26000mb',
  extended: true,
}));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  secret: 'live-trim-session-parameter',
  resave: true,
  saveUninitialized: true,
  cookie: { maxAge: 1209600000 }, // two weeks in milliseconds
}));

app.use('/v1', router);
app.use('/', index);
app.use('/config', config);
// app.use('/testimdb', imdb);

app.use((req, res, next) => {
  //console.log(req.session.user_id);
  if (req.session.user_id == null) {
    // if user is not logged-in redirect back to login page //
    res.redirect('/');
  } else {
    next();
  }
});


app.use('/v1/users', users);
app.use('/v1/video', video);
app.use('/v1/dashboard', dashboard);
// app.use('/v1/profile', profile);
app.use('/v1/settings', settings);
app.use('/v1/admin', admin);
app.use('/v1/invoice', invoice);


// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use((err, req, res) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


mongoose.set('debug', true) // for dev only

/**
 * Start Express server.
 */
app.listen(app.get('port'), () => {
  console.log('%s App is running at http://localhost:%d in %s mode', chalk.green('✓'), app.get('port'), app.get('env'));
  console.log('  Press CTRL-C to stop\n');
});

module.exports = app;
