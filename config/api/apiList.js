module.exports = {

    'mcube_ui': {
        "base": {
            //'development': 'http://13.232.6.127:3000',
            'development': 'http://localhost:3000',
            'staging': 'https://mam.skandha.tv',
            'production': ''
        },
        'config': {
            'addTenantConfig': '/config/addTenantConfig',
            'deleteTenantConfig': '/config/deleteTenantConfig',
        }
    },

    'mcube_transcode': {
        "base": {
            //'development': 'http://13.232.6.127:3001',
            'development': 'http://localhost:3001',
            'staging': '',
            'production': ''
        },
        'config': {
            'addTenantConfig': '/config/addTenantConfig',
            'deleteTenantConfig': '/config/deleteTenantConfig',
        },
        'video': {

        },

        'dashboard': {
            'proccessingList': '/dashboard/proccessing_list',
            'todays_uploaded': '/dashboard/todays_uploaded',
            'proccessing_list': '/dashboard/proccessing_list',
            'transcoded_list': '/dashboard/transcoded_list'
        },



    },

    'mcube_adv_transcode': {
        "base": {
            //'development': 'http://13.232.6.127:3002',
            'development': 'http://localhost:3002',
            'staging': '',
            'production': ''
        },
        'config': {
            'addTenantConfig': '/config/addTenantConfig',
            'deleteTenantConfig': '/config/deleteTenantConfig',
        },
        'video': {
            'listS3_Assets': '/video/listS3_Assets'
        }
    },

    'mcube_cognito': {
        "base": {
            //'development': 'http://13.232.6.127:3003',
            'development': 'http://localhost:3003',
            'staging': '',
            'production': ''
        },
        'config': {
            'addTenantConfig': '/config/addTenantConfig',
            'deleteTenantConfig': '/config/deleteTenantConfig',
        }

    },

}