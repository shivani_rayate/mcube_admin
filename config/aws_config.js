module.exports = {
    development: { // CREDS - DEV
        accessKeyId: 'AKIAXGF633HYOXXBBS6R',
        secretAccessKey: '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',
        region: 'ap-south-1',
        bucket: 'dev-skandha-vod',
        putfolder: 'uploaded',
        getfolder: 'transcoded',
        getfolder_assets: 'assets',
        thumbnail_folder: 'thumbnails',

        SNS_CONFIG: {
            TopicArn: {
                preview: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-preview-sns',
                basic: 'arn:aws:sns:ap-south-1:494321523184:qa-dam-basic-sns',
                standard: 'arn:aws:sns:ap-south-1:494321523184:qa-dam-standard-sns',
                premium: 'arn:aws:sns:ap-south-1:494321523184:qa-dam-premium-sns',
            }
        },

        MEDIACONVERT_CONFIG: {
            accessKeyId: 'AKIAXGF633HYOXXBBS6R',
            secretAccessKey: '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',
            endpoint: 'https://idej2gpma.mediaconvert.ap-south-1.amazonaws.com',
            region: 'ap-south-1',
        },

        DISNEY_CONFIG: {
            bucket: 'transcoding-rawmedia',
            previewFolder: 'disney-low-resolution',
            delimiter: '/',
            dynamoDB_table: 'qa-dam-extract-lang',
            // TopicArn: 'arn:aws:sns:ap-south-1:705991774343:qa_disney_multi_lang_sns'
            TopicArn: 'arn:aws:sns:ap-south-1:705991774343:qa_multilang_dynamic_disney_sns_topic'
        },

        AZURE: {
            dynamoDB_table: 'qa-dam-content-metadata-db',
            TopicArn: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-content-metadata-sns'
        },

        // credentials for super admin app
           
         COGNITO: {
            UserPoolId: 'ap-south-1_tbnVg3LKq',
            ClientId: '3oh0sadguuhm8gt20os96ttjd'
        },


        video: {
            extension: '.mp4',
            index: 'index.m3u8',
        },

        DYNAMO_DB: {
            tables: {
                preview_transcode: 'qa-dam-preview',
                basic_transcode: 'qa-dam-basic-db',
                standard_transcode: 'qa-dam-standard-db',
                premium_transcode: 'qa-dam-premium-db',
                custom_transcode: 'qa-dam-custom-db'
            }
        },

        image: {
            extension: '.jpg',
        },

        IMDB: {
            apiKey: 'dbd0550e'
        },

        cloudformation: {
            mcube_transcode: 'qa-cloudformation-basic-model-lambda',
            mcube_adv_transcode: 'qa-adv-transcoding-cloudformation-lambda',
            mcube_data_science: 'qa-data-science-cloudformation-lambda',
            configDynamodbTable: 'qa-dam-tenantInfo', // config info of tenants
            deleteStackLambda: 'qa-cloudformation-delete-stack-lambda-function',
        },

    },

    production: { // CREDS - PROD

        SNS_CONFIG: {
            TopicArn: {
                preview: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-preview-sns',
                basic: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-basic-sns',
                standard: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-standard-sns',
                premium: 'arn:aws:sns:ap-south-1:705991774343:qa-dam-premium-sns',
            }
        },

    },
};