const mongoose = require('mongoose');
const moment = require('moment');

const ProfileSchema = mongoose.Schema({
    content_id: { type: String },
    profile_name: { type: String, required: true },
    profiles: { type: Object, require: true },
    mediaJSON: { type: Object, require: true },
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
}, { toJSON: { virtuals: true } });

ProfileSchema.pre('save', function (next) {
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});

module.exports = mongoose.model('Profiles', ProfileSchema, 'Profiles');