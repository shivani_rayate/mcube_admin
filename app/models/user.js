// load the things we need
const mongoose = require('mongoose');
//const bcrypt = require('bcrypt');

// define the schema for our user model
const userSchema = mongoose.Schema({
    fullname: { type: String },
    email: { type: String, unique: true },
    password: { type: String },
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
    updated_by: { type: String },
});


userSchema.pre('save', function (next) {

    // if created_at doesn't exist, add to that field
    if (this.isNew) {
        console.log('IS NEW CALLED userSchema!!');

        //let pass = this.password;
        // var hash = bcrypt.hashSync(pass, 8);

        //this.password = hash;
        this.created_at = new Date();
        this.updated_at = new Date();
        next();
    } else {
        console.log('IS NEW IS FALSE userSchema!!');
        this.updated_at = new Date();
        this.updated_by = this.user_id;
        next();
    }
});

// checking if password is valid
//userSchema.methods.validPassword = function (password) {
//  return bcrypt.compareSync(password, this.local.password);
//};


userSchema.post('save', function (doc) {

});

// create the model for users and expose it to our app
module.exports = mongoose.model('user', userSchema, 'user');
