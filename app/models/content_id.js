const mongoose = require('mongoose');

// eslint-disable-next-line prefer-destructuring
const Schema = mongoose.Schema;

// eslint-disable-next-line no-unused-vars
const awsConfig = require('../../config/aws_config')[process.env.NODE_ENV];

const CotentSchema = new Schema({
    content_id: { type: String, unique: true },
    // contentid_series: { type: Number, required: true },
    created_at: Date,
    updated_at: Date,
}, { toJSON: { virtuals: true } });


// on every save, add the date
// eslint-disable-next-line func-names
CotentSchema.pre('save', function (next) {
    // if created_at doesn't exist, add to that field
    if (this.isNew) {
        // this.status = false;
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
    } else {
        console.log(' IS NEW IS FALSE!! ');
        this.updated_at = new Date();
    }
    next();
});
// Post Save - Configure Redis
// CotentSchema.post('save', function (doc) {
// });

// var Video = mongoose.model('video_prod', video_schema);//videos,video_test,video_prod
const Contentid = mongoose.model('Contentid', CotentSchema, 'Contentid');

module.exports = Contentid;
