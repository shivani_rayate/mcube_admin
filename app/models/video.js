const mongoose = require('mongoose');
const moment = require('moment');

// eslint-disable-next-line prefer-destructuring
const Schema = mongoose.Schema;

const VideoSchema = new Schema({
    content_id: { type: String },
    file_name: { type: String },
    video_id: { type: String },
    duration: { type: String },
    title: { type: String },
    summary_short: { type: String },
    media_job_status: { type: String, default: 'PROGRESSING' },
    media_job_id: { type: String, default: '' },
    preview_url: { type: String, default: 'NA' },
    video_path: { type: String, default: 'NA' },

    thumbnail: [{ type: String }],
    thumbnail_pic: { type: String },
    thumbnail_resolution: [{ type: String }], // 1920*1080 , Other res.

    created_date: { type: Date, default: moment().format('YYYY-MM-DD') },
    created_at: Date,
    updated_at: Date,
    created_by: { type: String },
    keywords: { type: String },
    language: { type: String },
    description: { type: String },
    cast: { type: String },
    is_featured: { type: Boolean, default: false },
    is_popular: { type: Boolean, default: false },

    profiles: {
        basic: {
            url: { type: String, default: 'NA' },
            status: { type: String, default: 'NA' },
        },
        standard: {
            url: { type: String, default: 'NA' },
            status: { type: String, default: 'NA' },
        },
        premium: {
            url: { type: String, default: 'NA' },
            status: { type: String, default: 'NA' },
        },
        custom: {
            url: { type: String, default: 'NA' },
            status: { type: String, default: 'NA' },
        }
    }

}, { toJSON: { virtuals: true } });

VideoSchema.pre('save', function (next) {
    // change the updated_at field to current date
    // if created_at doesn't exist, add to that field
    if (this.isNew) {
        console.log(' IS NEW CALLED!! ');
        this.created_at = new Date();
        this.updated_at = new Date();
        this.start_date = new Date();
    } else {
        console.log(' IS NEW IS FALSE !!! ');
        this.updated_at = new Date();
    }
    next();
});

const Video = mongoose.model('Video', VideoSchema, 'Video');

module.exports = Video;