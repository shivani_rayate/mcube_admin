const AWS = require('aws-sdk');
const awsConfig = require('../../../../config/aws_config')['development'];

/**
 * CHECK MEDIA CONVERTER STATUS WITH JOBID
 * @param {*OBJECT WITH INPUTFILENAME AND ID} reqParams
 * @param {*RETURN WITH STATUS OF MEDIA CONVERTER} callback
 */


exports.getMediaConvertStatus = reqParams => new Promise((resolve, reject) => {

    AWS.config.update({
        accessKeyId: awsConfig.MEDIACONVERT_CONFIG.accessKeyId,
        secretAccessKey: awsConfig.MEDIACONVERT_CONFIG.secretAccessKey,
        region: awsConfig.MEDIACONVERT_CONFIG.region,
        endpoint: awsConfig.MEDIACONVERT_CONFIG.endpoint,
    });

    const mediaconvert = new AWS.MediaConvert();
    /**
    * @param - mediaParams is required for mediaconvert get job API
    */
    const mediaParams = {
        Id: reqParams,
    };
    mediaconvert.getJob(mediaParams, (errGetJob, data) => {
        // mediaconvert.getJob(mediaParams, function(errGetJob, data) {
        if (errGetJob) {
            console.log(`mediaconvert getJob ERROR> ${errGetJob}`);
            reject(errGetJob); // an error occurred
        } else {
            console.log(`****************** GET JOB ********** :${reqParams} >>>  ${data.Job.Status}`); //  response from media convert
            if (data && data.Job && data.Job.Status) {
                if (data.Job.Status === 'COMPLETE') {
                    console.log(`****************** GET JOB COMPLETE **********_`)
                    let obj = {};
                    obj.status = 200;
                    obj.data = data;
                    obj.message = 'COMPLETE'
                    resolve(obj)
                }else if (data.Job.Status === 'PROGRESSING'){
                    let obj = {};
                    obj.status = 201;
                    obj.data = data;
                    obj.message = 'PROGRESSING'
                    resolve(obj)
                }
            } else {
                reject();
            }
        }
    });
});