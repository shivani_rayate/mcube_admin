const AWS = require('aws-sdk');
const util = require('util');
// eslint-disable-next-line no-undef
const awsConfig = require('../../config/aws_config')[ENV];
const s3Copy = require('../utilities/s3_copy');
// eslint-disable-next-line import/order
const _ = require('lodash');

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});
const s3 = new AWS.S3();

/**
 *
 * @param {*VIDEO OBJECT} reqParams
 * @param {*RETURN WITH ARRAY OF } callback
 */
exports.updateThumbnail = reqParams => new Promise((resolve, reject) => {
    const params = {
        Bucket: awsConfig.bucket,
        // MaxKeys: 2,
        // Delimiter: "/",
        // Prefix: awsConfig.getfolder+"/assets/"+reqParams.content_id+"/Thumbnails/",
        Prefix: `${awsConfig.getfolder}/${awsConfig.getfolder_assets}/${reqParams.content_id}/${awsConfig.thumbnail_folder}/`,
    };
    console.log(`UPDATE THUMBNAIL CALLED !! ${util.inspect(params)}`);
    const resultArray = [];
    //    s3.listObjects(params, function(err, data) {
    s3.listObjects(params, (err, data) => {
        if (err) {
            reject();
        } else {
            const arrKeys = _.map(data.Contents, 'Key');
            console.log(`arrKeys> ${util.inspect(arrKeys)}`);
            // let idx= 0;
            // eslint-disable-next-line no-undef
            arrKeys.forEach((element, idx) => {
                const val = element.replace(`${awsConfig.getfolder}/${awsConfig.getfolder_assets}/${reqParams.content_id}/${awsConfig.thumbnail_folder}/`, '');
                // console.log(val +'i: '+ idx + ' element : '+ arrKeys.length);
                const s3CopyParams = {
                    NEW_KEY: val,
                    Bucket: `${awsConfig.bucket}/${awsConfig.getfolder}/${awsConfig.getfolder_assets}/${reqParams.content_id}/${awsConfig.thumbnail_folder}`,
                    Copy_Source: `${awsConfig.bucket}/${awsConfig.getfolder}/${awsConfig.getfolder_assets}/${reqParams.content_id}/${awsConfig.thumbnail_folder}/${val}`,
                    // Copy_Source : awsConfig.bucket+'/'+ awsConfig.getfolder +'/assets/'+
                     // reqParams.content_id +'/Thumbnails/'+ val,
                };

                s3Copy.s3Copy(s3CopyParams);
                resultArray.push(val);

                if (idx === arrKeys.length - 1) {
                    resolve(resultArray);
                    // callback(null, result_arry);
                }
                // idx++;
            });
        }
    });
});

exports.listS3Data = reqParams => new Promise((resolve, reject) => {
    const params = {
        Bucket: awsConfig.bucket,
        // MaxKeys: 2,
        Delimiter: reqParams.Delimiter ? '/' : undefined,
        // Prefix: awsConfig.getfolder+"/assets/"+reqParams.content_id+"/Thumbnails/",
        Prefix: reqParams.Prefix,
    };
    console.log(`listS3Data params: ${JSON.stringify(params)}`);
    s3.listObjects(params, (err, data) => {
        if (err) {
            reject(err);
        } else {
            resolve(data);
        }
    });
});

exports.s3Copy = reqParams => new Promise((resolve, reject) => {
    const params = {
        Bucket: awsConfig.bucket,
        // MaxKeys: 2,
        // Delimiter: "/",
        // Prefix: awsConfig.getfolder+"/assets/"+reqParams.content_id+"/Thumbnails/",
        Prefix: reqParams.Prefix,
    };
    s3.listObjects(params, (err, data) => {
        if (err) {
            reject(err);
        } else {
            resolve(data);
        }
    });
});
