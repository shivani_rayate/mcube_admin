var AWS = require('aws-sdk');

const util = require('util');
// eslint-disable-next-line no-undef
const awsConfig = require('../../config/aws_config')['development'];

AWS.config.update({

    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
    
});

const s3 = new AWS.S3();

exports.listS3Assets = reqParams => new Promise((resolve, reject) => {

    var params = {
        Bucket: reqParams.bucket ? reqParams.bucket : '',
        Delimiter: '/',
        Prefix: `adv-transcode/input-data/`,
        // Prefix: ' ',
        // Marker:' ',
        // MaxKeys: ' ',
    };
    s3.listObjectsV2(params, function (err, data) {
        if (err) console.log(err, err.stack); // an error occurred
        else {
           //  console.log("listS3Assets Data >> >> >>  " + JSON.stringify(data));
            resolve(data)
        }
    });

})


exports.listS3AssetsDynamodb = reqParams => new Promise((resolve, reject) => {

    // ************************** //
    delete AWS.config['endpoint']; // ISSUE IS MC
    // ************************** //

    var docClient = new AWS.DynamoDB.DocumentClient();


    var params = {
        TableName: awsConfig.DISNEY_CONFIG.dynamoDB_table,
    };

    docClient.scan(params, onScan);

    function onScan(err, data) {
        if (err) {
            console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
            reject(err)
        } else {

            // console.log("Scan succeeded." + JSON.stringify(data));
            resolve(data)

        }
    }

})

//////// /////// ################ %% CONTENT METADATA %% ################    ///////////

exports.listS3ContentMetadatatDynamodb = reqParams => new Promise((resolve, reject) => {

    var db = new AWS.DynamoDB();

    var params = {

        TableName: awsConfig.DISNEY_CONFIG.dynamoDB_table,
        Key: {
            "assetid": { S: reqParams.assetid }
        }
    };

    // console.log("data get FINALLY > ", JSON.stringify(params))

    db.getItem(params, function (err, data) {
        if (err) {
            console.log(err); // an error occurred
        }
        else {
            //console.log(data); // successful response
            resolve(data);
        }

    });

})



//////// /////// ################ %% SNS TRIGGERERING %% ################    ///////////

// create preview of S3 List Asset

exports.create_preview_asset = reqParams => new Promise((resolve, reject) => {

    var sns = new AWS.SNS();

    var sns_params = {
        Message: JSON.stringify({ content_path: reqParams.content_path }),
        TopicArn: TopicArn
    };

    // console.log("sns params >> " + JSON.stringify(sns_params))

    sns.publish(sns_params, function (err, data) {
        if (err) {
            console.log("create preview Publish ERROR >", err, err.stack); // an error occurred
            reject(err)
        } else {
            console.log("create preview Publish SUCCESS for > ");
        }
    });

})

// getlink-preview-asset

exports.getlink_preview_asset = reqParams => new Promise((resolve, reject) => {
    var params = {
        Bucket: awsConfig.bucket,
        Key:awsConfig.DISNEY_CONFIG.previewFolder+reqParams.key,
        Expires:60 * 5,
    }
    s3.getSignedUrl('getObject', params, function (err, url) {
        if (err) {
            console.log('The error is', err);
            reject(err)
        } else {
            console.log('The URL is', url);
            resolve(url)
        }

    });
   
});



// To insert into DynamoDB
exports.updateDynamodb = (reqParams) => {
    return new Promise((resolve, reject) => {

        var docClient = new AWS.DynamoDB.DocumentClient()

        var params = {
            TableName: awsConfig.DISNEY_CONFIG.dynamoDB_table,
            Key: {
                'assetid': reqParams.assetid
            },
            UpdateExpression: "set audio_info = :a",
            ExpressionAttributeValues: {
                ":a": reqParams.audio_info
            },
            ReturnValues: "UPDATED_NEW"
        }

        docClient.update(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject()
            }
            else {
                // console.log("updateDynamodbPromise succeeded for Update Audio status:." + JSON.stringify(data));
                resolve()
            }
        });

    })
};


// Video transcode_asset
exports.transcode_asset = reqParams => new Promise((resolve, reject) => {
    var sns = new AWS.SNS();

    // console.log("HERE reqParams "+ JSON.stringify(reqParams));

    var sns_params = {
        Message: JSON.stringify(reqParams),
        TopicArn: awsConfig.DISNEY_CONFIG.TopicArn
    };

    // console.log("HERE NOW HERE "+ JSON.stringify(sns_params));

    sns.publish(sns_params, function (err, data) {
        if (err) {
            console.log("transcode_asset Publish ERROR >", err, err.stack); // an error occurred
            reject(err)
        } else {
            console.log("SNS transcode success")
            resolve("SNS Published Successfully!")
        }
    });
})

// create meta data AZURE
exports.create_meta_data_azure = reqParams => new Promise((resolve, reject) => {
    var sns = new AWS.SNS();

    var sns_params = {
        Message: JSON.stringify(reqParams),
        TopicArn: awsConfig.AZURE.TopicArn
    };

    //console.log("HERE NOW create_meta_data_azure " + JSON.stringify(sns_params))

    sns.publish(sns_params, function (err, data) {
        if (err) {
            console.log("create_meta_data_azure Publish ERROR >", err, err.stack); // an error occurred
            reject(err)
        } else {
            resolve("All SNS Published Successfully!")
        }
    });
})


// get meta data AZURE
exports.get_meta_data_azur = reqParams => new Promise((resolve, reject) => {

    var db = new AWS.DynamoDB();

    var params = {

        TableName: awsConfig.AZURE_METADATA.dynamoDB_table,
        Key: {
            "assetid": { S: reqParams.assetid }
        }
    };

    // console.log("data get FINALLY > ", JSON.stringify(params))

    db.getItem(params, function (err, data) {
        if (err) {
            console.log(err); // an error occurred
        }
        else {
            //console.log(data); // successful response
            resolve(data);
            //console.log("data get FINALLY > ", JSON.stringify(data))
        }

    });
})