const express = require('express');
const router = express.Router();
const AWS = require('aws-sdk');
const awsConfig = require('../../../config/aws_config')['development'];

var MongoClient = require('mongodb').MongoClient;


exports.createMongodb = reqParams => new Promise((resolve, reject) => {
  //console.log(" createMongodb database >> ");
  var url = `mongodb://localhost:27017/${reqParams.dbName}`;
 //var url=`mongodb+srv://suvarna:suvarna@1997@cluster0.7xugz.mongodb.net/${reqParams.dbName}?retryWrites=true&w=majority`
  MongoClient.connect(url, function (err, db) {
    if (err) {
      console.log(err)
      reject(err);
    } else {
      var data = db.db(reqParams.dbName);
      data.createCollection("Video", function (err, res) {
        if (err) {
          console.log(err)
          reject(err);
        } else {
          //console.log("\n Mongo DB and Collection created!");
          db.close();
          let obj = {
            status: 200,
            message: "Mongo Db created!"
          }
          resolve(obj)
        }
      });
    }
  });

});