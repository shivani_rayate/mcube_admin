const express = require('express');
const router = express.Router();

const imdb = require('imdb-api')
const imdbdata = require('../../../workers/testimdb');
const imdbObj = new imdbdata.ImdbClass();
const AWS = require('aws-sdk');

const awsConfig = require('../../../config/aws_config')['development'];

// AWS.config.update({
//     accessKeyId: awsConfig.accessKeyId,
//     secretAccessKey: awsConfig.secretAccessKey,
//     region: awsConfig.region,
// });

//////// /////// ################ %% SNS TRIGGERERING %% ################    ///////////

exports.getImdbData = reqParams => new Promise((resolve, reject) => {

    //console.log("getImdbData here  >> ");

    imdb.get({ name: reqParams.name ? reqParams.name : null, }, { apiKey: awsConfig.IMDB.apiKey, timeout: 30000 }).then((data) => {

        if (data) {

            //console.log("DATA FROM IMDB HERE >> " + JSON.stringify(data));

            const params = {
                assetid: reqParams.assetid,
                metadata: data
            };

            imdbObj.save(params).then((response) => {

                if (response.status === 200) {
                    let obj = {};
                    obj.status = 200;
                    obj.data = response.data;
                    obj.message = 'Imdb Meta Data fetched'
                    resolve(obj);

                } else {
                    reject()
                }
            })
        } else {
            //console.log("else from IMDB HERE >> ");
            reject()
        }
    }).catch((err) => {
        //console.log("Exception IN IMDB >> " + JSON.stringify(err))
        let obj = {};
        obj.status = 404;
        obj.data = null;
        obj.message = 'Movie not found!'
        resolve(obj);
    });
})


// To insert into DynamoDB
exports.updateImdbStatus = (reqParams) => {
    return new Promise((resolve, reject) => {

        var docClient = new AWS.DynamoDB.DocumentClient()

        var params = {
            TableName: awsConfig.DISNEY_CONFIG.dynamoDB_table,
            Key: {
                'assetid': reqParams.assetid
            },
            UpdateExpression: "set imdb_data_status = :a",
            ExpressionAttributeValues: {
                ":a": reqParams.imdb_data_status
            },
            ReturnValues: "UPDATED_NEW"
        }

        docClient.update(params, function (err, data) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject()
            }
            else {
                // console.log("updateImdbStatus succeeded for Update Audio status:." + JSON.stringify(data));
                resolve()
            }
        });

    })
};
