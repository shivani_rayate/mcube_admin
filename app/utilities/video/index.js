/* eslint-disable prefer-arrow-callback */
/* eslint-disable no-unused-vars */
/* eslint-disable func-names */
/* eslint-disable no-unused-expressions */
/* eslint-disable operator-linebreak */
/* eslint-disable prefer-template */
/* eslint-disable prefer-destructuring */
/* eslint-disable arrow-body-style */
const moment = require('moment');
const _ = require('lodash');
const util = require('util');
const fs = require('fs');
const AWS = require('aws-sdk');
// eslint-disable-next-line import/no-self-import

const videoPartial = require('../../../app/utilities/video/index');

//const Video = require('../../../workers/video');

let video_utility = require('../../../app/utilities/video');

//const videoObj = new Video.VideoClass();

const awsConfig = require('../../../config/aws_config')['development'];

const listS3Image = require('../../../app/utilities/listS3Image');

exports.getNextContentId = (paramsReq) => {
    // eslint-disable-next-line no-unused-vars
    return new Promise((resolve, reject) => {
        const actualNumber = (paramsReq.count + 1).toString();
        console.log(`ACTUAL NUMBER> ${actualNumber}`);
        let zeroPreceeding;
        if (actualNumber.length === 1) {
            zeroPreceeding = '000000';
        } else if (actualNumber.length === 2) {
            zeroPreceeding = '00000';
        } else if (actualNumber.length === 3) {
            zeroPreceeding = '0000';
        } else if (actualNumber.length === 4) {
            zeroPreceeding = '000';
        } else if (actualNumber.length === 5) {
            zeroPreceeding = '00';
        } else if (actualNumber.length === 6) {
            zeroPreceeding = '0';
        }
        const contentId = `${zeroPreceeding}${actualNumber}`;
        resolve(contentId);
    });
};

exports.mediaGetJob = (params) => {
    return new Promise((resolve, reject) => {
        // console.log(`MEDIA CONVERT access_key: ${awsConfig.mediaconvert_config.accessKeyId}`);
        // console.log(`MEDIA CONVERT secret_key > ${awsConfig.mediaconvert_config.secretAccessKey}`);
        // console.log(`MEDIA CONVERT endpoint: ${awsConfig.mediaconvert_config.endpoint} --MEDIA CONVERT region> ${awsConfig.mediaconvert_config.region}`);
        AWS.config.update({
            // accessKeyId: awsConfig.mediaconvert_config.accessKeyId,
            // secretAccessKey: awsConfig.mediaconvert_config.secretAccessKey,
            // region: awsConfig.mediaconvert_config.region,
            endpoint: awsConfig.MEDIACONVERT_CONFIG.endpoint,
        });
        const mediaconvert = new AWS.MediaConvert();
        const mediaParams = {
            Id: params.jobId,
        };
        // media convert end
        mediaconvert.getJob(mediaParams, (errGetJob, data) => {
            // mediaconvert.getJob(mediaParams, function(errGetJob, data) {
            if (errGetJob) {
                console.log(`mediaconvert getJob ERROR> ${errGetJob}`);
                reject(errGetJob); // an error occurred
            } else {
                resolve(data);
            }
        });
    });
};

exports.mediaCreateJob = (params) => {

    return new Promise((resolve, reject) => {
        const resObj = {};
        AWS.config.update({
            endpoint: awsConfig.MEDIACONVERT_CONFIG.endpoint,
        });
        const mediaconvert = new AWS.MediaConvert();
        // Create a promise on a MediaConvert object
        const templateJobPromise = mediaconvert.createJob(params).promise();

        // // Handle promise's fulfilled/rejected status
        templateJobPromise.then(
            (data) => {
                console.log(`data.Job.Id GENERATED> ${data.Job.Id}`);
                resObj.status = 200;
                resObj.JobId = data.Job.Id;
                resolve(resObj);
            },
            (err) => {
                console.log(`ERR IN MEDIA CONVERT ${err}`);
                resObj.status = 500;
                resObj.JobId = null;
                resolve(resObj);
            },
        );
    });
};

exports.processMediaConvertV2 = (reqParams) => {
    return new Promise((resolve, reject) => {
        //console.log('##################### HIT MEDIA SERVICE V2 #######################');
        // media_job_id
        const mediaJobParams = {
            jobId: reqParams.media_job_id,
        };
        videoPartial.mediaGetJob(mediaJobParams).then((data) => {
            // eslint-disable-next-line no-param-reassign
            reqParams.media_job_status = data.Job.Status;
            console.log(`reqParams.media_job_status>> ${reqParams.media_job_status}`);
            if (reqParams.media_job_status === 'COMPLETE' || reqParams.media_job_status === 'ERROR') {
                console.log('JOB COMPLETE!!!');

                // videoPartial.listS3AndGetThumbnail(reqParams).then((thumbnails) => {
                //     // eslint-disable-next-line no-unused-vars
                //     const updateParamsThumbnail = {
                //         content_id: reqParams.content_id,
                //         thumbnail: (thumbnails || []),
                //         thumbnail_pic: (reqParams.media_job_status === 'COMPLETE' && thumbnails.length < 3 ? thumbnails[0] : (thumbnails[2] || '')),
                //         media_job_status: reqParams.media_job_status,
                //     };
                //     // eslint-disable-next-line max-len
                //     videoObj.findOneAndUpdate(updateParamsThumbnail).then((rs) => {
                //         console.log('VIDEO UPDATE COMPLETE!!!!');
                //     });
                // });


                // ************************** //
                delete AWS.config['endpoint']; // ISSUE IS MC
                // ************************** //


                // read status in DYNAMODB
                let _params = {
                    tableName: awsConfig.DYNAMO_DB.tables.custom_transcode,
                    contentId: reqParams.contentId
                };
                video_utility.getDynamodbStatus(_params);

            } else {
                setTimeout(() => {
                    videoPartial.processMediaConvertV2(reqParams);
                }, 2000);
            }
        });
    });
};


// list s3 for thumbnails
exports.listS3AndGetThumbnail = reqParams => new Promise((resolve, reject) => {
    if (reqParams.media_job_status === 'COMPLETE') {
        const s3Params = {
            Bucket: `${awsConfig.bucket}`,
            Prefix: `${awsConfig.getfolder}/${awsConfig.getfolder_assets}/${reqParams.content_id}/${awsConfig.thumbnail_folder}/`,
        };
        console.log(`UPDATE THUMBNAIL CALLED !! ${util.inspect(s3Params)}`);
        listS3Image.listS3Data(s3Params).then((data) => {
            const resultArray = [];
            const arrKeys = _.map(data.Contents, 'Key');
            console.log(`arrKeys> ${util.inspect(arrKeys)}`);
            _.forEach(arrKeys, (element, idx) => {
                const val = element.split('/').reverse()[0];
                console.log(`ALLELEMENTKEYS>>> ${val}`);
                const s3CopyParams = {
                    NEW_KEY: val,
                    Bucket: `${awsConfig.bucket}/${awsConfig.getfolder}/${awsConfig.getfolder_assets}/${reqParams.content_id}/${awsConfig.thumbnail_folder}`,
                    Copy_Source: `${awsConfig.bucket}/${awsConfig.getfolder}/${awsConfig.getfolder_assets}/${reqParams.content_id}/${awsConfig.thumbnail_folder}/${val}`,
                    // Copy_Source : awsConfig.bucket+'/'+ awsConfig.getfolder +'/assets/'+
                    // reqParams.content_id +'/Thumbnails/'+ val,
                };
                listS3Image.s3Copy(s3CopyParams);

                resultArray.push(val);
                if (idx === arrKeys.length - 1) {
                    resolve(resultArray);
                }
            });
        });
    } else {
        resolve('');
    }
});