const AWS = require('aws-sdk');

const awsConfig = require('../../../config/aws_config')['development'];

AWS.config.update({
    accessKeyId: awsConfig.accessKeyId,
    secretAccessKey: awsConfig.secretAccessKey,
    region: awsConfig.region,
});


const API = require('../../../config/api/apiList');
const BASE_URL = API.mcube_ui.base.staging;

const cognitoidentityserviceprovider = new AWS.CognitoIdentityServiceProvider();

const mongodb = require('../../../app/utilities/mongodb/create');

// Create Tenant
exports.createUserPoolApp = (paramsReq) => {
    console.log("\n \n #################### \n pool name in cognito >>> " + JSON.stringify(paramsReq))
    return new Promise((resolve, reject) => {

        var params = {
            PoolName: paramsReq.poolName,
            AccountRecoverySetting: {
                RecoveryMechanisms: [
                    {
                        Name: 'admin_only' /* required */,
                        Priority: 1 /* required */
                    }
                ]
            },
            AdminCreateUserConfig: {
                AllowAdminCreateUserOnly: true,
                InviteMessageTemplate: {
                    EmailMessage: `Discover more about Skandha Media Manager !!! <br>
                    <br>
                    <img class="img-fluid" src="https://media-exp1.licdn.com/dms/image/C4E0BAQF6MaCpykbWtg/company-logo_200_200/0?e=2159024400&v=beta&t=TAf6_KoXe2P-nTBpGAg9qggzZEOeLsLO4MOLhVkXcMc" alt="SKANDHA"> <br>
                    Tenant ID : ${paramsReq.poolName} <br>
                    Username : {username} <br>
                    Temporary Password : {####} <br>
                    To explore more about Skandha Media Manager verify your account:
                    <a href="${BASE_URL}/authenticateUser">Verify Here!!</a><br>
                    <br>
                    Cheers, <br>
                    Skandha Team<br>
                    Website: <a href="https://www.skandha.in">https://www.skandha.in</a>`,
                    EmailSubject: `Welcome to Skandha Media Manager!!`,
                    SMSMessage: `Your username is {username} and temporary password is {####}.`
                },
                UnusedAccountValidityDays: 30
            },
            // SmsConfiguration: {
            //     SnsCallerArn: 'STRING_VALUE' /* required */
            // },
            UserPoolAddOns: {
                AdvancedSecurityMode: 'AUDIT' /* required */
            }
        }
        //console.log("user pool create params > " + JSON.stringify(params))
        cognitoidentityserviceprovider.createUserPool(params, function (err, userPoolData) {
            if (err) {
                console.log('Err in create user pool is ' + JSON.stringify(err));
                reject(err)
            } else {

                // Create User Pool Client
                let params = {
                    ClientName: userPoolData.UserPool.Name,
                    UserPoolId: userPoolData.UserPool.Id,
                    GenerateSecret: false,
                    RefreshTokenValidity: 30, // 'NUMBER_VALUE' days
                }
                console.log(" create user pool client name >> " + JSON.stringify(params))
                cognitoidentityserviceprovider.createUserPoolClient(params, function (err, userPoolClientData) {
                    if (err) {
                        console.log(err, err.stack); // an error occurred
                        reject(err)
                    } else {

                        // Create Mongo DB
                        let params = {
                            dbName: userPoolData.UserPool.Name
                        }
                        mongodb.createMongodb(params).then((response) => {
                            if (response.status === 200) {
                                let obj = {
                                    userPoolData: userPoolData,
                                    userPoolClientData: userPoolClientData,
                                    mongodb: params.dbName
                                }
                                resolve(obj)
                            } else {
                                reject()
                            }
                        })
                    }
                });

            }
        });
    })
};


// list user pool
exports.listUserPools = (paramsReq) => {
    return new Promise((resolve, reject) => {

        var params = {
            MaxResults: paramsReq.maxResults,
        }
        cognitoidentityserviceprovider.listUserPools(params, function (err, data) {
            if (err) {
                reject(err)
            } else {
                // console.log('list user pool data is ' + JSON.stringify(data));
                resolve(data)
            }
        });
    })
};


// Describe user pool data
exports.describeUserPool = (paramsReq) => {
    return new Promise((resolve, reject) => {

        var params = {
            UserPoolId: paramsReq.userPoolId,
        }
        // console.log("describe user pool params > " + JSON.stringify(params))
        cognitoidentityserviceprovider.describeUserPool(params, function (err, data) {
            if (err) {
                reject(err)
            } else {
                //console.log('describe user pool data is ' + JSON.stringify(data));
                resolve(data)
            }
        });
    })
};


// Register Tenant Admin
exports.RegisterUser = (paramsReq) => {
    return new Promise((resolve, reject) => {

        var params = {
            UserPoolId: paramsReq.userPoolId, /* required */
            Username: paramsReq.email, /* required */
            TemporaryPassword: paramsReq.password,
            DesiredDeliveryMediums: [
                'EMAIL'
            ],
            ForceAliasCreation: false,
            // MessageAction: 'SUPPRESS',
            UserAttributes: [
                {
                    Name: 'email', /* required */
                    Value: paramsReq.email
                },
                {
                    Name: 'name', /* required */
                    Value: paramsReq.name
                }
                /* more items */
            ]
        };

        // 1. Create User
        cognitoidentityserviceprovider.adminCreateUser(params, function (err, adminUserData) {
            if (err) {
                console.log(err, err.stack); // an error occurred
                reject(err)
            } else {
                //console.log('adminUserData is ' + JSON.stringify(adminUserData));

                // 2. Create Group
                let params = {
                    GroupName: 'Admin',
                    UserPoolId: paramsReq.userPoolId,
                }
                cognitoidentityserviceprovider.createGroup(params, function (err, adminGroupData) {
                    if (err) {
                        console.log(err);
                        reject(err)
                    } else {
                        //  console.log("adminGroupData  >> ", JSON.stringify(adminGroupData));

                        // 3. Add user to the Admin Group
                        let params = {
                            GroupName: 'Admin',
                            UserPoolId: paramsReq.userPoolId, // Your user pool id here
                            Username: paramsReq.email,
                        }
                        cognitoidentityserviceprovider.adminAddUserToGroup(params, function (err, result) {
                            if (err) {
                                console.log(err);
                                reject(err)
                            } else {
                                //console.log("addUser To Group >> ", JSON.stringify(result));

                                let obj = {
                                    adminUserData: adminUserData,
                                    adminGroupData: adminGroupData,
                                }
                                resolve(obj)
                            }

                        });

                    }

                });

            }
        });
    })
};


// get Admin user
exports.listUsersInGroup = (paramsReq) => {
    return new Promise((resolve, reject) => {
        var params = {

            UserPoolId: paramsReq.userPoolId, /* required */
            GroupName: 'Admin',
            Limit: '10',
            // NextToken: "string",

        }
        console.log("params >> ", JSON.stringify(params));
        cognitoidentityserviceprovider.listUsersInGroup(params, function (err, result) {
            if (err) {
                console.log(err);
                reject(err)
            } else {
                // console.log("ListUsersInGroup >> ", JSON.stringify(result));
                resolve(result)
            }

        });

    })
}

// Delete Tenant 
exports.deleteUserPool = (paramsReq) => {
    console.log("pool name in cognito in deleteUserpool >>> " + JSON.stringify(paramsReq))

    return new Promise((resolve, reject) => {

        var params = {
            UserPoolId: paramsReq.userPoolId, // Your user pool id here 
        }
        cognitoidentityserviceprovider.deleteUserPool(params, function (err, result) {
            if (err) {
                console.log("deleteUserPool ERROR >> " + err);
                reject(err)
            } else {
                resolve(result)
            }

        });

    })
}